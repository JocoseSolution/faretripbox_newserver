﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="FltResIntl.aspx.vb" Inherits="FltResIntl"
    MasterPageFile="~/MasterAfterLogin.master" %>

<%@ Register Src="~/UserControl/FltSearchmdf.ascx" TagPrefix="uc1" TagName="FltSearch" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <link href="../Styles/flightsearch.css" rel="stylesheet" type="text/css" />
    <%-- <link href="../Styles/main.css" rel="stylesheet" />--%>
    <link href="../Styles/jquery-ui-1.8.8.custom.css" rel="stylesheet" type="text/css" />
             <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

    <link href="../Custom_Design/css/my_design.css" rel="stylesheet" />

       <link href="../icofont/icofont.css" rel="stylesheet" />
    <link href="../icofont/icofont.min.css" rel="stylesheet" />

    <style type="text/css">


.one-way-select {
            position: fixed;
            bottom: 0;
            z-index: 999999;
            background: #eee;
            width: 100%;
            left: 0;
            right: 0;
            padding: 25px;
            box-shadow: 0px 1px 10px 1px rgb(6 6 6 / 58%);
        }


        .spnBtnShow {
    background: #ffcfcf;
    padding: 2px;
    border-radius: 2px;
}

        .spnBtnHide {
    background: #cfffd7;
    padding: 2px;
    border-radius: 2px;
}

    .falsebookbutton {
    display: inline-block;
    background: #e60000;
    border: #ff3131;
    color: #fff;
    text-align: center;
    padding: 10px;
    border-radius: 5px;
    }
    .falsebookbutton:hover {
        background:#797979;
        border:#797979;
    }

    .buttonfltbk {
            display: inline-block;
    background: #e60000;
    border: #ff3131;
    color: #fff;
    text-align: center;
    padding: 10px;
    border-radius: 5px;
    }

        .buttonfltbk:hover {
            background:#797979;
        border:#797979;
        }
  #loading-msg {
            width: 100%;
            position: absolute;
            left: 0;
            bottom: 220px;
            text-align: center;
            
            color: #333;
            
        }


</style>


     <style type="text/css">
        
.tabs {
  max-width: 100% !important;
}
.tabs-nav li {
  float: left !important;
  width: 25% !important;
  
}
.tabs-nav li:first-child a {
  border-right: 0 !important;
  border-top-left-radius: 6px !important;
}
.tabs-nav li:last-child a {
  border-top-right-radius: 6px !important;
}
.tb-btn {
  background: #eaeaed;
  border: 1px solid #cecfd5;
  color: #0087cc;
  display: block;
  font-weight: 600;
  padding: 10px 0;
  text-align: center;
  text-decoration: none;
}
.tb-btn:hover {
  color: #ff7b29;
}
.tab-active ,.tb-btn {
  background: #fff !important;
  border-bottom-color: transparent !important;
  color: #2db34a !important;
  cursor: default !important;
}
.tabs-stage {
  /*border: 1px solid #cecfd5 !important;*/
  border-radius: 0 0 6px 6px !important;
  border-top: 0 !important;
  clear: both !important;
  /*padding: 24px 30px !important;*/
  position: relative !important;
  top: -1px !important;
}

    </style>


 <style type="text/css">
     .modal-window {
  position: fixed;
  background-color: rgb(0 0 0 / 25%);
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  z-index: 999;
  visibility: hidden;
  opacity: 0;
  pointer-events: none;
  -webkit-transition: all 0.3s;
  transition: all 0.3s;
}
.modal-window:target {
  visibility: visible;
  opacity: 1;
  pointer-events: auto;
}
.modal-window > div {
  width: 100%;
  position: absolute;
  top: 50%;
  left: 50%;
  -webkit-transform: translate(-50%, -50%);
          transform: translate(-50%, -50%);
  padding: 2em;
  background: #ffffff;
}
.modal-window header {
  font-weight: bold;
}
.modal-window h1 {
  font-size: 150%;
  margin: 0 0 15px;
}

.modal-close {
  color: #aaa;
  line-height: 50px;
  font-size: 80%;
  position: absolute;
  right: 0;
  text-align: center;
  top: 0;
  width: 70px;
  text-decoration: none;
}
.modal-close:hover {
  color: black;
}

/* Demo Styles */



a {
  color: inherit;
}



.modal-window div:not(:last-of-type) {
  margin-bottom: 15px;
}

small {
  color: #aaa;
}

.btn {
  background-color: #e20000;
  padding: 1em 1.5em;
  border-radius: 3px;
  text-decoration: none;
}
.btn i {
  padding-right: 0.3em;
}

 </style>

    <style type="text/css">
        .backdrop {
            background-color: white;
            position: fixed;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
        }

        .spinner {
            position: absolute;
            top: calc(50% - 12.5px);
            left: calc(50% - 25px);
            width: 50px;
            height: 50px;
            border-top: 8px solid aliceblue;
            border-right: 8px solid aliceblue;
            border-bottom: 8px solid aliceblue;
            border-left: 8px solid #8c618d;
            border-radius: 50%;
            animation-name: spin;
            animation-duration: 3s;
            animation-iteration-count: infinite;
            animation-timing-function: linear;
        }

        @keyframes spin {
            0% {
                transform: rotate(0deg);
                border-left: 8px solid deeppink;
            }

            25% {
                transform: rotate(360deg);
                border-left: 8px solid gold;
            }

            50% {
                transform: rotate(720deg);
                border-left: 8px solid palegreen;
            }

            75% {
                transform: rotate(1080deg);
                border-left: 8px solid aqua;
            }

            100% {
                transform: rotate(1440deg);
                border-left: 8px solid deeppink;
            }
        }

        .logo {
            position: absolute;
            top: calc(50% + 35px);
            left: calc(50% - 25px);
            font-family: sans-serif;
            color: gray;
            letter-spacing: 0.1em;
        }
    </style>





    <style>
        label {
            /*color: orange;*/
        }

        table {
            width: 100%;
            border-collapse: collapse;
            /*margin: 50px auto;*/
            height: 119px;
        }

        tr:nth-of-type(odd) {
            background: #eee;
        }

        th {
            background: #0952a4;
            color: #a5a4a4;
            /*font-weight: bold;*/
        }

        td, th {
            padding: 2px;
            border: 1px solid #ccc;
            text-align: left;
            /*font-size: 18px;*/
        }


        @media only screen and (max-width: 760px), (min-device-width: 768px) and (max-device-width: 1024px) {

            table {
                width: 100%;
            }


            table, thead, tbody, th, td, tr {
                display: block;
                padding: inherit;
            }


                thead tr {
                    position: absolute;
                    top: -9999px;
                    left: -9999px;
                }

            tr {
                border: 1px solid #ccc;
            }

            td {
                border: none;
                border-bottom: 1px solid #eee;
                position: relative;
                padding-left: 50%;
            }

                td:before {
                    position: absolute;
                    top: 6px;
                    left: 6px;
                    width: 45%;
                    padding-right: 10px;
                    white-space: nowrap;
                    content: attr(data-column);
                    color: #000;
                    font-weight: bold;
                }
        }
    </style>


    <style type="text/css">
        .fltkun {
            position: relative;
            top: -17px;
            float: left;
            right: 571px;
            width: 501px;
        }

        @media only screen and (min-device-width: 1440px) and (max-device-width: 1919px) {

            .fltkun {
                position: relative;
                top: -17px;
                float: left;
                right: 655px !important;
                width: 501px;
            }
        }


        @media only screen and (min-device-width: 1600px) and (max-device-width: 1919px) {

            .fltkun {
                position: relative;
                top: -17px;
                float: left;
                right: 735px !important;
                width: 501px;
            }
        }



        .fltkun2 {
            position: relative;
            top: 10px;
            float: left;
            right: 87%;
            width: 460px;
        }

        @media only screen and (min-device-width: 1440px) and (max-device-width: 1919px) {

            .fltkun2 {
                position: relative;
                top: 10px;
                float: left;
                right: 99% !important;
                width: 460px;
            }
        }


        @media only screen and (min-device-width: 1600px) and (max-device-width: 1919px) {

            .fltkun2 {
                position: relative;
                top: 10px;
                float: left;
                right: 109% !important;
                width: 460px;
            }
        }


        .fltkun3 {
            position: relative;
            top: 10px;
            float: left;
            right: 79%;
            width: 460px;
        }

        @media only screen and (min-device-width: 1440px) and (max-device-width: 1919px) {

            .fltkun3 {
                position: relative;
                top: 10px;
                float: left;
                right: 89% !important;
                width: 460px;
            }
        }


        @media only screen and (min-device-width: 1600px) and (max-device-width: 1919px) {

            .fltkun3 {
                position: relative;
                top: 10px;
                float: left;
                right: 100% !important;
                width: 460px;
            }
        }
    </style>









    <style type="text/css">
        #flterTab {
            padding-left: 0px !important;
        }

        input[type=checkbox] {
            margin-top: 7px;
        }

        body {
            font-family: "Lato", sans-serif;
        }

        ul.tab {
            list-style-type: none;
            margin: 0;
            padding: 0;
            overflow: hidden;
            border: 1px solid #ccc;
            background-color: #f1f1f1;
        }
            /* Float the list items side by side */
            ul.tab li {
                float: left;
                display: inline;
            }
                /* Style the links inside the list items */
                ul.tab li a {
                    display: inline-block;
                    color: black;
                    text-align: center;
                    padding: 14px 16px;
                    text-decoration: none;
                    transition: 0.3s;
                    font-size: 17px;
                }
                    /* Change background color of links on hover */
                    ul.tab li a:hover {
                        background-color: #ddd;
                    }
                    /* Create an active/current tablink class */
                    ul.tab li a:focus, .active {
                        /*background-color: #ccc;*/
                    }

        /* Style the tab content */
        .tabcontent {
            /*display: none;*/
            padding: 6px 12px;
            border: 1px solid #ccc;
            border-top: none;
            width: 98%;
        }

        .tablinks {
            font-size: 11px;
            font-weight: 600;
        }

        .des-nav {
            z-index: 999;
            position: fixed;
            top: 135px;
            left: 21.5%;
            font-size: 13px;
            width: 76%;
            border: 1px solid #d1d1d1;
            border-bottom: none;
            border-top: none;
        }
    </style>




    <style type="text/css">
        .wrapper {
            /*text-transform: uppercase;*/
            /*background: #ececec;*/
            color: #555;
            cursor: help;
            /*font-family: "Gill Sans", Impact, sans-serif;*/
            /*font-size: 20px;*/
            /*margin: 100px 75px 10px 75px;
padding: 15px 20px;
position: relative;
text-align: center;*/
            /*width: 200px;*/
            -webkit-transform: translateZ(0); /* webkit flicker fix */
            -webkit-font-smoothing: antialiased; /* webkit text rendering fix */
        }

            .wrapper .tooltip-content {
                background: #005999;
                /*font-size: 14px;*/
                font-weight: 100;
                bottom: 100%;
                color: #fff;
                display: block;
                /*line-height: 2px;*/
                left: -128px;
                /*top: -45px;*/
                margin-bottom: 8px;
                opacity: 0;
                padding: 12px;
                pointer-events: none;
                position: absolute;
                width: 153%;
                -webkit-transform: translateY(10px);
                -moz-transform: translateY(10px);
                -ms-transform: translateY(10px);
                -o-transform: translateY(10px);
                transform: translateY(10px);
                -webkit-transition: all .25s ease-out;
                -moz-transition: all .25s ease-out;
                -ms-transition: all .25s ease-out;
                -o-transition: all .25s ease-out;
                transition: all .25s ease-out;
                -webkit-box-shadow: 2px 2px 6px rgba(0, 0, 0, 0.28);
                -moz-box-shadow: 2px 2px 6px rgba(0, 0, 0, 0.28);
                -ms-box-shadow: 2px 2px 6px rgba(0, 0, 0, 0.28);
                -o-box-shadow: 2px 2px 6px rgba(0, 0, 0, 0.28);
                box-shadow: 2px 2px 6px rgba(0, 0, 0, 0.28);
            }

                /* This bridges the gap so you can mouse into the tooltip without it disappearing */
                .wrapper .tooltip-content:before {
                    bottom: -20px;
                    content: " ";
                    display: block;
                    height: 20px;
                    left: 0;
                    position: absolute;
                    width: 100%;
                }

                /* CSS Triangles - see Trevor's post */
                .wrapper .tooltip-content:after {
                    border-left: solid transparent 10px;
                    border-right: solid transparent 10px;
                    border-top: solid #005999 10px;
                    bottom: -10px;
                    content: " ";
                    height: 0;
                    left: 69%;
                    margin-left: -13px;
                    position: absolute;
                    width: 0;
                }

            .wrapper:hover .tooltip-content {
                opacity: 1;
                pointer-events: auto;
                -webkit-transform: translateY(0px);
                -moz-transform: translateY(0px);
                -ms-transform: translateY(0px);
                -o-transform: translateY(0px);
                transform: translateY(0px);
            }

        /* IE can just show/hide with no transition */
        .lte8 .wrapper .tooltip-content {
            display: none;
        }

        .lte8 .wrapper:hover .tooltip-content {
            display: block;
        }
    </style>


    <style type="text/css">
        .wrapper-1 {
            text-transform: uppercase;
            /*background: #ececec;*/
            color: #555;
            cursor: help;
            /*font-family: "Gill Sans", Impact, sans-serif;*/
            font-size: 20px;
            /*margin: 100px 75px 10px 75px;
padding: 15px 20px;
position: relative;
text-align: center;*/
            /*width: 200px;*/
            -webkit-transform: translateZ(0); /* webkit flicker fix */
            -webkit-font-smoothing: antialiased; /* webkit text rendering fix */
        }

            .wrapper-1 .tooltip-content-1 {
                width: 220%;
                position: absolute;
                /* bottom: 100%; */
                margin: 0 0 7px 0;
                /*padding: 15px;*/
                font-weight: normal;
                font-style: normal;
                text-align: CENTER;
                text-decoration: none;
                text-shadow: 0 1px 0 rgba(255,255,255,0.3);
                /*line-height: 1.5;*/
                /*border: solid 1px;*/
                /*-moz-border-radius: 7px;
    -webkit-border-radius: 7px;*/
                /*border-radius: 7px;*/
                -moz-box-shadow: 0 1px 2px rgba(0,0,0,0.3), 0 1px 2px rgba(255,255,255,0.5) inset;
                -webkit-box-shadow: 0 1px 2px rgba(0,0,0,0.3), 0 1px 2px rgba(255,255,255,0.5) inset;
                box-shadow: 0 1px 2px rgba(0,0,0,0.3), 0 1px 2px rgba(255,255,255,0.5) inset;
                cursor: default;
                display: block;
                /* visibility: hidden; */
                color: #fff;
                background: #005999;
                padding: 7px;
                left: -118px;
                opacity: 0;
                z-index: 999;
                -moz-transition: all 0.4s linear;
                -webkit-transition: all 0.4s linear;
                -o-transition: all 0.4s linear;
                transition: all 0.4s linear;
            }

                /* This bridges the gap so you can mouse into the tooltip without it disappearing */
                .wrapper-1 .tooltip-content-1:before {
                    bottom: -20px;
                    content: " ";
                    display: block;
                    height: 20px;
                    left: 0;
                    position: absolute;
                    width: 100%;
                }

                /* CSS Triangles - see Trevor's post */
                .wrapper-1 .tooltip-content-1:after {
                    border-left: solid transparent 10px;
                    border-right: solid transparent 10px;
                    border-top: solid #005999 10px;
                    bottom: -10px;
                    content: " ";
                    height: 0;
                    left: 68%;
                    /* margin-left: -13px; */
                    position: absolute;
                    width: 0;
                    top: -10px;
                    transform: rotate(180deg);
                }

            .wrapper-1:hover .tooltip-content-1 {
                opacity: 1;
                pointer-events: auto;
                -webkit-transform: translateY(0px);
                -moz-transform: translateY(0px);
                -ms-transform: translateY(0px);
                -o-transform: translateY(0px);
                transform: translateY(0px);
            }

        /* IE can just show/hide with no transition */
        .lte8 .wrapper-1 .tooltip-content-1 {
            display: none;
        }

        .lte8 .wrapper-1:hover .tooltip-content-1 {
            display: block;
        }
    </style>


    <style type="text/css">
        .wrapper-1 {
            text-transform: uppercase;
            /*background: #ececec;*/
            color: #555;
            cursor: help;
            /*font-family: "Gill Sans", Impact, sans-serif;*/
            font-size: 20px;
            /*margin: 100px 75px 10px 75px;
padding: 15px 20px;
position: relative;
text-align: center;*/
            /*width: 200px;*/
            -webkit-transform: translateZ(0); /* webkit flicker fix */
            -webkit-font-smoothing: antialiased; /* webkit text rendering fix */
        }

        .wrapper-2 .tooltip-content-2 {
            background: #005999;
            /*font-size: 14px;*/
            font-weight: 100;
            bottom: 100%;
            color: #fff;
            display: block;
            line-height: 2px;
            left: -64px;
            top: -50px;
            margin-bottom: 15px;
            opacity: 0;
            padding: 20px;
            pointer-events: none;
            position: absolute;
            width: 176%;
            -webkit-transform: translateY(10px);
            -moz-transform: translateY(10px);
            -ms-transform: translateY(10px);
            -o-transform: translateY(10px);
            transform: translateY(10px);
            -webkit-transition: all .25s ease-out;
            -moz-transition: all .25s ease-out;
            -ms-transition: all .25s ease-out;
            -o-transition: all .25s ease-out;
            transition: all .25s ease-out;
            -webkit-box-shadow: 2px 2px 6px rgba(0, 0, 0, 0.28);
            -moz-box-shadow: 2px 2px 6px rgba(0, 0, 0, 0.28);
            -ms-box-shadow: 2px 2px 6px rgba(0, 0, 0, 0.28);
            -o-box-shadow: 2px 2px 6px rgba(0, 0, 0, 0.28);
            box-shadow: 2px 2px 6px rgba(0, 0, 0, 0.28);
        }

            /* This bridges the gap so you can mouse into the tooltip without it disappearing */
            .wrapper-2 .tooltip-content-2:before {
                bottom: -20px;
                content: " ";
                display: block;
                height: 20px;
                left: 0;
                position: absolute;
                width: 100%;
            }

            /* CSS Triangles - see Trevor's post */
            .wrapper-2 .tooltip-content-2:after {
                border-left: solid transparent 10px;
                border-right: solid transparent 10px;
                border-top: solid #005999 10px;
                bottom: -10px;
                content: " ";
                height: 0;
                left: 69%;
                margin-left: -13px;
                position: absolute;
                width: 0;
            }

        .wrapper-2:hover .tooltip-content-2 {
            opacity: 1;
            pointer-events: auto;
            -webkit-transform: translateY(0px);
            -moz-transform: translateY(0px);
            -ms-transform: translateY(0px);
            -o-transform: translateY(0px);
            transform: translateY(0px);
        }

        /* IE can just show/hide with no transition */
        .lte8 .wrapper-2 .tooltip-content-2 {
            display: none;
        }

        .lte8 .wrapper-2:hover .tooltip-content-2 {
            display: block;
        }
    </style>



    <%--<style type="text/css">
        input[type=checkbox] {
            margin-top: 7px;
        }
        fltrDiv
        body {
            font-family: "Lato", sans-serif;
        }

        ul.tab {
            list-style-type: none;
            margin: 0;
            padding: 0;
            overflow: hidden;
            border: 1px solid #ccc;
            background-color: #f1f1f1;
        }
            /* Float the list items side by side */
            ul.tab li {
                float: left;
                display: inline;
            }
                /* Style the links inside the list items */
                ul.tab li a {
                    display: inline-block;
                    color: black;
                    text-align: center;
                    padding: 14px 16px;
                    text-decoration: none;
                    transition: 0.3s;
                    font-size: 17px;
                }
                    /* Change background color of links on hover */
                    ul.tab li a:hover {
                        background-color: #ddd;
                    }
                    /* Create an active/current tablink class */
                    /*ul.tab li a:focus, .active {
                        background-color: #ccc;
                    }*/

        /* Style the tab content */
        .tabcontent {
            /*display: none;*/
            padding: 6px 12px;
            border: 1px solid #ccc;
            border-top: none;
        }
    </style>--%>



    <div class="theme-hero-area front">
        <div class="theme-hero-area-bg-wrap">
            <div class="theme-hero-area-bg theme-hero-area-bg-blur" style="background-color: #002646; filter: none"></div>
            <div class="theme-hero-area-mask theme-hero-area-mask-half"></div>
        </div>
        <div class="theme-hero-area-body" style="padding: 13px;">
            <div class="container">
                <div class="theme-search-area _mob-h theme-search-area-white">
                    <div class="row" data-gutter="10">
                        <div id="displaySearchinput" class="lft"></div>
                        <div class="col-md-2">
                            <a href="#" id="show-hide" class="theme-search-area-submit _tt-uc theme-search-area-submit-curved theme-search-area-submit-sm theme-search-area-submit-no-border theme-search-area-submit-primary" style="background: #e8e8e8; margin-top: 1px; color: #000;">Modify Search</a>
                         <div class="row mob-filter" style="position: absolute; right: 0; top: -25px;">
                                <div class="col-xs-2"><a href="#" id="show-hide-mob"><i class="icofont-ui-edit"></i></a></div>
                                <div class="col-xs-2"><a href="#" id="show-filter"><i class="icofont-filter"></i></a></div>
                            </div>
                        </div>
                    </div>
                    <div class="re-search" style="display: none; overflow: visible !important;">
                        <div id="fltrDiv">
                            <div id="searchtext" class="clear passenger">
                                <div class="container">
                                    <uc1:FltSearch runat="server" ID="FltSearch" />
                                </div>


                            </div>


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="w100   " id="toptop">
        <div id="MainSFR">
            <div id="MainSF">
                <%-- <div id="lftdv" class="hide" onclick="fltrclick1(this.id)">
                    <div class="collapse"><< Collapse</div>
                    <div class="collapse hide">>> Expand</div>
                </div>--%>

                <div class="w100">

                    <div class="row row-col-static" id="lftdv1" data-gutter="20" style="padding: 12px;">
                        <div id="">
                            <%-- <div class="clear"></div>
                            <div class="row">
                                   <input type="checkbox" class="chkNonstop" style="display:none" name="chkNonstop" id="chkNonstop" value="Y"/>
                                <div id="Modsearch" style="display: none; background: url(../images/blur-images-14.jpg); height: 100%; width: 100%; position: absolute; z-index: 99; left: 0; top: 0;">
                                    <div style="width: 85%; margin: 50px auto;">
                                        <div id="mdclose" style="width: 30px; height: 30px; border-radius: 50%; border: 3px solid #ccc; position: absolute; right: 10px; top: 10px; text-align: Center; font-size: 15px; background-color: black; cursor: pointer; color: #fff;">
                                            X
                                        </div>--%>

                            <%--</div>
                                </div>--%>
                            <%--  <div class="large-2 medium-3 small-6 columns passenger"><button type="button" class="jplist-reset-btn cursorpointer f16" data-control-type="reset" data-control-name="reset" data-control-action="reset" style="border: none; background: none;">
                                        Reset All Filters
                                        <img src="../images/reset.png" style="position: relative; top: 3px;" />
                                    </button></div> --%>
                            <%--</div>--%>



                            <%--<div class="bld f16">
                                <img src="../images/filter.png" />
                                Filter Search
                            </div>--%>


                            <div id="FilterLoad">
                            </div>
                            <div id="FilterBox">
                                <div class="jplist-panel">


                                    <div id="flterTab" style="display: none">
                                        <div id="flterTabO" class="spn1">
                                            Outbound
                                        </div>
                                        <div class="lft w2">&nbsp;</div>
                                        <div id="flterTabR" class="spn">
                                            Inbound
                                        </div>
                                    </div>


                                </div>
                            </div>
                        </div>


                        <div class="col-md-3">
                            <div class="sticky-col _mob-h">
                                <div class="theme-search-results-sidebar">
                                    <div class="theme-search-results-sidebar-sections _mb-20 _br-2 theme-search-results-sidebar-sections-white-wrap">


                                        <div id="dsplm" class="jplist-panel large-12 medium-12 small-12 columns">

                                            <a href="#" class="" data-control-type="reset" data-control-name="reset" data-control-action="reset" style="float: right; margin-right: 9px; color: orange;">Reset All </a>

                                        </div>

                                        <div class="jplist-panel">


                                            <div class="theme-search-results-sidebar-section" id="FltrPrice">
                                                <h5 class="theme-search-results-sidebar-section-title" onclick="fltrclick(this.id)" id="FBP">Price</h5>
                                                <div class="theme-search-results-sidebar-section-price">
                                                    <div id="FBP1" class="w98 lft">
                                                        <div class="clear2"></div>
                                                        <div class="fo">
                                                            <div class="clsone">
                                                                <div class="jplist-range-slider" data-control-type="range-slider" data-control-name="range-slider"
                                                                    data-control-action="filter" data-path=".price" style="margin-left: 10px;">
                                                                    <div class="clear1"></div>
                                                                    <div class="ui-slider w90 mauto" data-type="ui-slider">
                                                                    </div>
                                                                    <div class="clear1"></div>
                                                                    <div style="float: left">
                                                                        <span class="lft">₹&nbsp; <span class="value lft" data-type="prev-value"></span></span>
                                                                    </div>
                                                                    <div style="float: right">
                                                                       <span class="rgt">₹&nbsp;<span class="value rgt" data-type="next-value"></span></span>
                                                                    </div>
                                                                </div>
                                                                <div class="hidden" data-control-type="default-sort" data-control-name="sort" data-control-action="sort"
                                                                    data-path=".price" data-order="asc" data-type="number">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="fr">
                                                            <div class="jplist-range-slider" data-control-type="range-sliderR" data-control-name="range-slider"
                                                                data-control-action="filter" data-path=".price">
                                                                <div class="clear1"></div>
                                                                <div class="ui-slider w90 mauto" data-type="ui-slider">
                                                                </div>
                                                                <div class="clear1"></div>
                                                                <div class="lft w45">
                                                                    <span class="lft">
                                                                        <img src="../images/rs.png" style="height: 13px;" />&nbsp;</span>
                                                                    <span class="value lft" data-type="prev-value"></span>
                                                                </div>
                                                                <div class="rgt w45">
                                                                    <span class="value rgt" data-type="next-value"></span>
                                                                    <span class="rgt">
                                                                        <img src="../images/rs.png" class="rgt" style="height: 13px;" />&nbsp;</span>
                                                                </div>
                                                            </div>
                                                            <div class="hidden" data-control-type="default-sort" data-control-name="sort" data-control-action="sort"
                                                                data-path=".price" data-order="desc" data-type="number">
                                                            </div>
                                                        </div>
                                                        <div class="clear">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>


                                            <hr />


                                            <div class="theme-search-results-sidebar-section" id="fltrTime">

                                                <h5 class="theme-search-results-sidebar-section-title" onclick="fltrclick(this.id)" id="FBDT">Departure Time
                                                </h5>
                                                <div id="FBDT1" class="w98 lft " style="overflow: hidden;">
                                                    <div class="fo">
                                                        <div class="jplist-group"
                                                            data-control-type="DTimefilterO"
                                                            data-control-action="filter"
                                                            data-control-name="DTimefilterO"
                                                            data-path=".dtime" data-logic="or">

                                                            <div class="row">
                                                                <div class="lft w20 abc1 bdrs">
                                                                    <input value="0_6" id="CheckboxT1" type="checkbox" title="Early Morning" style="display: none;" />
                                                                    <label for="CheckboxT1"></label>
                                                                    <%--<span>00 - 06</span>--%>
                                                                </div>
                                                                <div class="lft w20 abc2t bdrs">
                                                                    <input value="6_12" id="CheckboxT2" type="checkbox" title="Morning" style="display: none;" />
                                                                    <label for="CheckboxT2"></label>
                                                                    <%--<span>06 - 12</span>--%>
                                                                </div>

                                                                <div class="lft w20 abc3t bdrs">
                                                                    <input value="12_18" id="CheckboxT3" type="checkbox" title="Mid Day" style="display: none;" />
                                                                    <label for="CheckboxT3"></label>
                                                                    <%-- <span>12 - 18</span>--%>
                                                                </div>
                                                                <div class="lft w20 abc4t bdrs">
                                                                    <input value="18_0" id="CheckboxT4" type="checkbox" title="Evening" style="display: none;" />
                                                                    <label for="CheckboxT4"></label>
                                                                    <%--<span>18 - 00</span>--%>
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>
                                                    <div class="fr">
                                                        <div class="jplist-group"
                                                            data-control-type="DTimefilterR"
                                                            data-control-action="filter"
                                                            data-control-name="DTimefilterR"
                                                            data-path=".atime" data-logic="or">

                                                            <div class="lft w20 abc1 bdrs">
                                                                <input value="0_6" id="CheckboxT1R" type="checkbox" title="Early Morning" />
                                                                <label for="CheckboxT1"></label>
                                                                <span>00 - 06</span>
                                                            </div>
                                                            <div class="lft w20 abc2t bdrs">
                                                                <input value="6_12" id="CheckboxT2R" type="checkbox" title="Morning" />
                                                                <label for="CheckboxT2"></label>
                                                                <span>06 - 12</span>
                                                            </div>

                                                            <div class="lft w20 abc3t bdrs">
                                                                <input value="12_18" id="CheckboxT3R" type="checkbox" title="Mid Day" />
                                                                <label for="CheckboxT3"></label>
                                                                <span>12 - 18</span>
                                                            </div>
                                                            <div class="lft w20 abc4t bdrs">
                                                                <input value="18_0" id="CheckboxT4R" type="checkbox" title="Evening" />
                                                                <label for="CheckboxT4"></label>
                                                                <span>18 - 00</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="clear">
                                                    </div>
                                                </div>

                                            </div>


                                            <hr />

                                            <div class="theme-search-results-sidebar-section">
                                                <h5 class="theme-search-results-sidebar-section-title" onclick="fltrclick(this.id)" id="FBA">Airline
                                                </h5>
                                                <div class="w98 lft" id="FBA1">
                                                    <div class="clear2"></div>
                                                    <div id="airlineFilter" class="fo" style="overflow-y: scroll; height: 121px;"></div>
                                                    <div id="airlineFilterR" class="fr"></div>
                                                    <div class="clear"></div>
                                                </div>
                                                <div class="clear">
                                                </div>
                                            </div>



                                            <hr />

                                            <div class="theme-search-results-sidebar-section">
                                                <h5 class="theme-search-results-sidebar-section-title" onclick="fltrclick(this.id)" id="FBS">Stops
                                                </h5>
                                                <div class="w98 lft" id="FBS1">
                                                    <div id="stopFlter" class="fo"></div>
                                                    <div id="stopFlterR" class="fr"></div>
                                                    <div class="clear"></div>
                                                </div>
                                            </div>



                                            <hr />

                                            <div class="theme-search-results-sidebar-section">
                                                <h5 class="theme-search-results-sidebar-section-title" onclick="fltrclick(this.id)" id="FBFT">Fare Rule
                                                </h5>
                                                <div class="clsone" id="FBFT1">
                                                    <div class="w98 lft">
                                                        <div class="fo">
                                                            <div class="jplist-group"
                                                                data-control-type="RfndfilterO"
                                                                data-control-action="filter"
                                                                data-control-name="RfndfilteO"
                                                                data-path=".rfnd" data-logic="or">
                                                                <div class="clear"></div>
                                                                <div class="lft w15">
                                                                    <input value="r" id="CheckboxR1" type="checkbox" />
                                                                </div>
                                                                <div class="lft w80" style="padding-top: 3px">
                                                                    <label for="CheckboxR1">Refundable</label>
                                                                </div>
                                                                <div class="clear"></div>
                                                                <div class="lft w15">
                                                                    <input value="n" id="CheckboxR2" type="checkbox" />
                                                                </div>
                                                                <div class="lft w80" style="padding-top: 3px">
                                                                    <label for="CheckboxR2">Non Refundable</label>
                                                                </div>
                                                                <div class="clear"></div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>


                                            <hr />

                                            <div class="theme-search-results-sidebar-section">
                                                <h5 class="theme-search-results-sidebar-section-title" onclick="fltrclick(this.id)" id="DAFT">Airline Fare Type
                                                </h5>
                                                <div class="theme-search-results-sidebar-section-checkbox-list" id="DAFT1">
                                                    <div class="clear"></div>
                                                    <div id="AirlineFareType" class="FareTypeO theme-search-results-sidebar-section-checkbox-list"></div>
                                                    <div id="AirlineFareTypeR" class="FareTypeR theme-search-results-sidebar-section-checkbox-list"></div>
                                                    <div class="clear1">
                                                    </div>

                                                </div>
                                                <div class="clear">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6-5">
                            <div id="DivLoadP" class="w100" style="display: none;"></div>
                            <div id="" class="clear1 passenger" style="display: none;">
                                <div class="lft bld colormn hide">
                                    <div id="divSearchText1">
                                    </div>
                                </div>
                            </div>

                            <%-- <div class="w100 passenger">
                            <div class="lft padding1s cursorpointer clspMatrix" id="clspMatrix"><span style="padding-left:20px;">Matrix</span></div>
                            <div class="lft w80 hide" id="refinetitle"></div>
                        
                      

                        </div>--%>
                            <%--<div class="jplist-panel w5 rgt">
                            <div class="jplist-views"
                                data-control-type="views"
                                data-control-name="views"
                                data-control-action="views"
                                data-default="list-view">
                                <!-- data-default="list-view" -->
                                <button type="button" class="jplist-view list-view lft" style="background: url(../images/list-btn.png) no-repeat; height: 18px; width: 18px; border: none; cursor: pointer; margin-top: 3px;" title="Click to See Results in List View" data-type="list-view"></button>
                                <button type="button" class="jplist-view grid-view rgt" style="background: url(../images/grid-btn.png) no-repeat; height: 18px; width: 18px; border: none; cursor: pointer; margin-top: 3px;" title="Click to See Results in Grid View" data-type="grid-view"></button>
                            </div>
                        </div>--%>

                            <div class="jplist-panel">
                                <div id="divMatrixRtfO" class="divMatrix"></div>
                                <div id="divMatrixRtfR" class="hide divMatrix"></div>
                                <div id="divMatrix"></div>

                                <div class="w100 auto passenger" style="display: none;">
                                    <div class="w100 ptop10" style="background: #005999; height: 54px; width: 98%; padding: 1px 18px 0px 0px; border-radius: 4px; overflow: hidden; min-height: 60px; border: 1px solid #ccc;">
                                        <div class="lft" style="margin-top: 16px;">
                                            <div id="" class="lft">
                                            </div>
                                            <div class="lft" style="color: #fff; font-weight: 600; font-size: 14px; position: relative; left: 5px; top: 2px;">
                                             <%--   <span>|</span> <span id="spanShow" onclick="ShowHideDiscount('show');" class="spnBtnShow" style="cursor: pointer;">Show Net</span>
                                                <span id="spanHide" onclick="ShowHideDiscount('hide');" style="display: none;" class="spnBtnHide">Hide Net</span>--%>
                                            </div>

                                            <div class="rgt passenger" id="prexnt" style="margin-top: 20px; color: #000;">
                                                <div class="rgt" style="position: relative; left: 606px; top: -16px;">
                                                    <%-- <div class="auto lft">
                                                        
                                                    </div>--%>
                                                    <div class="auto lft">
                                                        <a href="#" id="PrevDay" class="pnday" style="color: #fff;">
                                                            <img src="../Styles/images/closeopen2.png" />&nbsp;Prev Day</a>
                                                    </div>
                                                    <div class="dotbdr lft">&nbsp;</div>
                                                    <div class="auto rgt"><a href="#" id="NextDay" class="pnday" style="color: #fff;">Next Day&nbsp;<img src="../Styles/images/closeopen.png" /></a></div>




                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                            </div>



                            <div id="RoundTripH" class="flightbox">
                                <div class="nav-container">

                                    <div class="jplist-panel">

                                        <div id="fltselct" class="hide">
                                            <div class="f16">Your Selection</div>
                                            <div id="selctcntnt" class="mauto">
                                                <div id="fltgo" class="w40 lft brdrtop">
                                                </div>
                                                <div class="w5 lft">&nbsp;</div>
                                                <div id="fltbk" class="w40 lft brdrtop">
                                                </div>
                                                <div id="fltbtn" class="w15 rgt">
                                                    <span class="f16 bld rgt" id="totalPay"></span>
                                                    <div class="clear"></div>
                                                    <input type="button" value="Book" class="buttonfltbk" id="FinalBook" />
                                                    <div id="Divproc" class="hide bld">
                                                        <img alt="Booking In Progress" width="50" height="50" src="~/Images/loading_bar.gif" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="clear">
                                            </div>
                                        </div>

                                        <div class="w100 mauto bld bgmn1 colorwht" style="display: none;">
                                            <div class="w50 padding1 lft">
                                                <div id="RTFTextFrom" class="lft f16"></div>
                                                <div class="rgt w5">&nbsp;</div>
                                                <div class="rgt">
                                                    <input type="button" id="RtfFromPrevDay" value="Prev Day" class="pnday f10 colorwht" />
                                                    |
                                                    <input type="button" id="RtfFromNextDay" value="Next Day" class="pnday f10 colorwht" />
                                                </div>
                                            </div>
                                            <div class="w45 padding1 lft">
                                                <div id="RTFTextTo" class="lft f16"></div>
                                                <div class="rgt w5">&nbsp;</div>
                                                <div class="rgt">
                                                    <input type="button" id="RtfToPrevDay" value="Prev Day" class="pnday f10 colorwht" />
                                                    |
                                                    <input type="button" id="RtfToNextDay" value="Next Day" class="pnday f10 colorwht" />
                                                </div>
                                            </div>
                                            <div class="clear">
                                            </div>
                                        </div>

                                        <div class="headerow hide">
                                            <div class="w10 colorwht lft srtarw" onclick="myfunction(this)">
                                                <div
                                                    class="hidden"
                                                    data-control-type="sortAirline"
                                                    data-control-name="sortAirline"
                                                    data-control-action="sort"
                                                    data-path=".airlineImage"
                                                    data-order="asc"
                                                    data-type="text">
                                                    Airline
                                                </div>
                                            </div>
                                            <div class="w10 colorwht lft srtarw" onclick="myfunction(this)">
                                                <div
                                                    class="hidden"
                                                    data-control-type="sortDeptime"
                                                    data-control-name="sortDeptime"
                                                    data-control-action="sort"
                                                    data-path=".deptime"
                                                    data-order="asc"
                                                    data-type="number">
                                                    Depart
                                                </div>

                                            </div>
                                            <div class="w10 colorwht lft srtarw" onclick="myfunction(this)">
                                                <div
                                                    class="hidden"
                                                    data-control-type="sortArrtime"
                                                    data-control-name="sortArrtime"
                                                    data-control-action="sort"
                                                    data-path=".arrtime"
                                                    data-order="asc"
                                                    data-type="number">
                                                    Arrival
                                                </div>

                                            </div>
                                            <div class="w10 colorwht lft srtarw" onclick="myfunction(this)">
                                                <div
                                                    class="hidden"
                                                    data-control-type="sortTotdur"
                                                    data-control-name="sortTotdur"
                                                    data-control-action="sort"
                                                    data-path=".totdur"
                                                    data-order="asc"
                                                    data-type="number">
                                                    Duration
                                                </div>
                                            </div>
                                            <div class="w10 colorwht lft srtarw" onclick="myfunction(this)">
                                                <div
                                                    class="hidden"
                                                    data-control-type="sortCITZ1"
                                                    data-control-name="sortCITZ1"
                                                    data-control-action="sort"
                                                    data-path=".price"
                                                    data-order="asc"
                                                    data-type="number">
                                                    Fare
                                                </div>
                                            </div>
                                            <div class="w10 colorwht lft srtarw" onclick="myfunction(this)">
                                                <div
                                                    class="hidden"
                                                    data-control-type="sortAirlineR"
                                                    data-control-name="sortAirlineR"
                                                    data-control-action="sort"
                                                    data-path=".airlineImage"
                                                    data-order="asc"
                                                    data-type="text">
                                                    Airline
                                                </div>
                                            </div>
                                            <div class="w10 colorwht lft srtarw" onclick="myfunction(this)">
                                                <div
                                                    class="hidden"
                                                    data-control-type="sortDeptimeR"
                                                    data-control-name="sortDeptimeR"
                                                    data-control-action="sort"
                                                    data-path=".deptime"
                                                    data-order="asc"
                                                    data-type="number">
                                                    Depart
                                                </div>
                                            </div>
                                            <div class="w10 colorwht lft srtarw" onclick="myfunction(this)">
                                                <div
                                                    class="hidden"
                                                    data-control-type="sortArrtimeR"
                                                    data-control-name="sortArrtimeR"
                                                    data-control-action="sort"
                                                    data-path=".arrtime"
                                                    data-order="asc"
                                                    data-type="number">
                                                    Arrival
                                                </div>
                                            </div>
                                            <div class="w10 colorwht lft srtarw" onclick="myfunction(this)">
                                                <div
                                                    class="hidden"
                                                    data-control-type="sortTotdurR"
                                                    data-control-name="sortTotdurR"
                                                    data-control-action="sort"
                                                    data-path=".totdur"
                                                    data-order="asc"
                                                    data-type="number">
                                                    Duration
                                                </div>
                                            </div>
                                            <div class="w10 colorwht lft srtarw" onclick="myfunction(this)">
                                                <div
                                                    class="hidden"
                                                    data-control-type="sortCITZR"
                                                    data-control-name="sortCITZR"
                                                    data-control-action="sort"
                                                    data-path=".price"
                                                    data-order="asc"
                                                    data-type="number">
                                                    Fare
                                                </div>
                                            </div>
                                            <div class="clear">
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div class="lft w51">
                                    <div id="divFrom1" class="listO w100">
                                        <%-- <div class="dvsrc">
                                        <img src="~/Images/fltloding.gif" />
                                    </div>--%>
                                    </div>
                                </div>
                                <div class="rgt w50">
                                    <div id="divTo1" class="listR w100">
                                        <%--<div class="dvsrc textaligncenter">
                                        <img src="~/Images/fltloding.gif" />
                                    </div>--%>
                                    </div>

                                    <%--<div class="dvsrc textaligncenter">
                                    <img src="~/Images/fltloding.gif" />
                                </div>--%>
                                </div>
                            </div>
                            <div class="flightbox" style="display: block;" id="onewayH">
                                <div class="nav-container">


                                    <div class="jplist-panel">

                                        <div class="headerow hide">
                                            <div class="w15 colorwht lft srtarw" onclick="myfunction(this)">
                                                <div
                                                    class="hidden"
                                                    data-control-type="sortAirline"
                                                    data-control-name="sortAirline"
                                                    data-control-action="sort"
                                                    data-path=".airlineImage"
                                                    data-order="asc"
                                                    data-type="text">
                                                    Airline
                                                </div>
                                            </div>
                                            <div class="w18 colorwht lft srtarw" onclick="myfunction(this)">

                                                <div
                                                    class="hidden"
                                                    data-control-type="sortDeptime"
                                                    data-control-name="sortDeptime"
                                                    data-control-action="sort"
                                                    data-path=".deptime"
                                                    data-order="asc"
                                                    data-type="number">
                                                    Departure
                                                </div>
                                            </div>
                                            <div class="w18 colorwht lft srtarw" onclick="myfunction(this)">

                                                <div
                                                    class="hidden"
                                                    data-control-type="sortArrtime"
                                                    data-control-name="sortArrtime"
                                                    data-control-action="sort"
                                                    data-path=".arrtime"
                                                    data-order="asc"
                                                    data-type="number">
                                                    Arrival
                                                </div>

                                            </div>
                                            <div class="w10 colorwht lft srtarw" onclick="myfunction(this)">

                                                <div
                                                    class="hidden"
                                                    data-control-type="sortTotdur"
                                                    data-control-name="sortTotdur"
                                                    data-control-action="sort"
                                                    data-path=".totdur"
                                                    data-order="asc"
                                                    data-type="number">
                                                    Duration
                                                </div>
                                            </div>
                                            <div class="w24 lft">
                                                &nbsp;
                                            </div>
                                            <div class="w15 colorwht rgt srtarw" onclick="myfunction(this)">
                                                <div
                                                    class="hidden"
                                                    data-control-type="sortCITZ"
                                                    data-control-name="sortCITZ"
                                                    data-control-action="sort"
                                                    data-path=".price"
                                                    data-order="asc"
                                                    data-type="number">
                                                    Fare
                                                </div>
                                            </div>
                                            <div class="clear">
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <div class="nav">

                                    <div class="jplist-panel">

                                        <div class="theme-search-results-sort _mob-h clearfix">
                                            <h5 class="theme-search-results-sort-title net-fare">Sort by:</h5>
                                            <ul class="theme-search-results-sort-list">

                                                <li class="abdd srtarw net-fare" onclick="myfunction(this)">
                                                    <a href="#" class="" data-control-type="sortAirline" data-control-name="sortAirline" data-control-action="sort" data-path=".airlineImage" data-order="asc" data-type="text">Airline 
                                                        <span>A &rarr; Z</span>
                                                    </a>
                                                </li>

                                                <li class="abdd srtarw net-fare" onclick="myfunction(this)">

                                                    <a href="#" class=""
                                                        data-control-type="sortDeptime"
                                                        data-control-name="sortDeptime"
                                                        data-control-action="sort"
                                                        data-path=".deptime"
                                                        data-order="asc"
                                                        data-type="number">Departure <span>Min-Time &rarr; Max-Time</span>
                                                    </a>
                                                </li>

                                                <li class="abdd srtarw net-fare" onclick="myfunction(this)">

                                                    <a href="#"
                                                        class=""
                                                        data-control-type="sortArrtime"
                                                        data-control-name="sortArrtime"
                                                        data-control-action="sort"
                                                        data-path=".arrtime"
                                                        data-order="asc"
                                                        data-type="number">Arrival <span>Min-Time &rarr; Max-Time</span>
                                                    </a>

                                                </li>



                                                <li class="abdd srtarw net-fare" onclick="myfunction(this)">

                                                    <a href="#"
                                                        class=""
                                                        data-control-type="sortTotdur"
                                                        data-control-name="sortTotdur"
                                                        data-control-action="sort"
                                                        data-path=".totdur"
                                                        data-order="asc"
                                                        data-type="number">Duration <span>Short &rarr; Long</span>
                                                    </a>
                                                </li>



                                                <li class="abdd srtarw hidden-xs net-fare" onclick="myfunction(this)">
                                                    <a href="#"
                                                        class=""
                                                        data-control-type="sortCITZ"
                                                        data-control-name="sortCITZ"
                                                        data-control-action="sort"
                                                        data-path=".price"
                                                        data-order="asc"
                                                        data-type="number">Fare <span>Low &rarr; High</span>
                                                    </a>
                                                </li>
                                            </ul>
                                            <div class="auto lft" style="margin-top: 8px;float: right;">
                                            <span id="spanShow" onclick="ShowHideDiscount('show');" class="spnBtnShow" style="cursor: pointer;">Show Net</span>
                                                <span id="spanHide" onclick="ShowHideDiscount('hide');" style="display: none;" class="spnBtnHide">Hide Net</span>
                                                </div>
                                        </div>
                                    </div>

                                </div>

                                <div id="mainDiv">
                                    <div id="divResult">
                                        <div id="divFrom" class="list" style="width: 100%;">
                                        </div>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                                <%-- <div class="jplist-no-results jplist-hidden">
                                <div class='clear1'></div>
                                <div class='clear1'></div>
                                <div class='w90 mauto padding1 brdr'>
                                    <div class='clear1'></div>
                                    <div class='clear1'></div>
                                    <span class='vald f20'>Sorry, we could not find a match for your query. Please modify your search.</span> &nbsp;<span onclick='DiplayMsearch(this.id);' class='underlineitalic cursorpointer'>Modify Search</span><div class='clear'></div>
                                </div>
                            </div>--%>
                            </div>
                        </div>

                    </div>

                </div>
            </div>
        </div>

        <div id="render">
        </div>

    </div>
    <%--<div id="fltselct" class="hide">
        <div class="clear1"></div>
        <div id="totalPay" class="f16">
            Your Selection
            <div class="clear1"></div>
        </div>
        <div id="selctcntnt" class="w70 mauto">
            <div class="clear1">
            </div>
            <div id="fltgo" class="w50 lft">
            </div>
            <div id="fltbk" class="w45 rgt">
            </div>
            <div id="fltbtn">
                <input type="button" value="Book" class="button1 hide" id="FinalBook" />
                <div id="Divproc" class="hide bld">
                    <img alt="Booking In Progress" src="~/Images/loading_bar.gif" />
                </div>
            </div>
        </div>
    </div>--%>
    <%--   </div>--%>
    <div id="waitMessage" style="display: none;">
      
            <%-- <h1 class="text-center" style="font-size: 20px;">PLEASE WAIT</h1>
            <span>Please do not close or refresh this window.</span><br />
            <img alt="loading" width="50" height="50" src="../images/loadingAnim.gif" />
            <br />
            <div id="searchquery" style="color: #004b91; padding-top: 15px">
            </div>--%>

   


         

          <div style="text-align: center; z-index: 101111111111111; font-size: 12px; font-weight: bold; padding: 20px;">

            <div class="backdrop">

                 <div id="searchquery" style="color: #000; font-size: 18px;text-align: center;margin-top: 140px;">
            </div>

                <div class="spinner"></div>
                <span id="loading-msg"></span>
            </div>
           




        </div>



  
    </div>

    <%-- <div id="divMail">
        <a href="#" class="topopup pop_button1" id="btnFullDetails">Mail All Result</a>
        <a href="#" class="topopup pop_button1" id="btnSendHtml">Mail Selected Result</a>
    </div>--%>
    <div id="backgroundPopup">
    </div>
    <div id="toPopup" class="flight_head">
        <div class="close">
        </div>
        <span class="ecs_
            ">Press Esc to close <span class="arrow"></span></span>
        <div id="popup_content">
            <!--your content start-->
            <table cellpadding="3" cellspacing="3">
                <tr>
                    <td colspan="2">
                        <h4 style="text-align: center; color: #FFFFFF; background-color: #20313f; font-weight: bold; padding-top: 5px; padding-bottom: 5px;">Send Mail</h4>
                    </td>
                </tr>
                <tr>
                    <td class="textsmall" style="width: 120px; padding-left: 10px;"></td>
                    <td class="textsmall">
                        <input type='radio' name='choices' checked="checked" value='A' />
                        All Result
                         <input type='radio' name='choices' value='S' />Selected Result
                    </td>
                </tr>

                <tr>
                    <td class="textsmall" style="width: 120px; padding-left: 10px;">From:
                    </td>
                    <td>
                        <input type="text" class="headmail" id="txtFromMail" name="txtFromMail" />
                    </td>
                </tr>
                <tr>
                    <td class="textsmall" style="width: 120px; padding-left: 10px;">To:
                    </td>
                    <td>
                        <input type="text" class="headmail" id="txtToMail" name="txtToMail" />
                    </td>
                </tr>
                <tr>
                    <td class="textsmall" style="width: 120px; padding-left: 10px;">Subject:
                    </td>
                    <td>
                        <input type="text" class="headmail" id="txtSubj" name="txtSubj" />
                    </td>
                </tr>
                <tr>
                    <td class="textsmall" style="width: 120px; padding-left: 10px;">Message:
                    </td>
                    <td>
                        <textarea id="txtMsg" class="headmail" name="txtMsg" rows="4" cols="20"></textarea>
                    </td>
                </tr>
                <tr>
                    <td style="margin-left: 20px;"></td>
                    <td align="right">
                        <%--<input type="button" class="pop_button" id="btnCancel" name="btnCancel" value="Cancel" />--%>
                        <input type="button" class="buttonfltbk" id="btnSendMail" name="btnSendMail" value="Send Details" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <%--<div id="divabc">&nbsp;</div>--%>
                        <label id="lblMailStatus" style="display: none; color: Red;">
                        </label>
                    </td>
                </tr>
            </table>
        </div>
        <!--your content end-->
    </div>
    <div id="FareBreakupHeder" class="modal" tabindex="-1" role="dialog" aria-labelledby="FareBreakupHederLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="FareBreakupHederLabel">Fare Summary</h5>
                    <button type="button" class="close FareBreakupHederClose" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>

                </div>
                <div class="modal-body" id="FareBreakupHederId" style="padding: 2px 10px">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary FareBreakupHederClose">Close</button>
                </div>
            </div>
        </div>
    </div>
    <%--    <div id="ConfmingFlight1" class="CfltFare" style="text-align: center; z-index: 1001; background-color: #f9f9f9; font-size: 12px; font-weight: bold; padding: 20px; box-shadow: 0px 1px 5px #000; border: 1px solid #d1d1d1; border-radius: 0px; display: none;">--%>

    <%--  <div id="Div1" style="display: none;">
        <div style="text-align: center; z-index: 1001; font-size: 12px; font-weight: bold; padding: 20px; border-radius: 0px;">
        <div id="divLoadcf" class="CfltFare1">
        
        </div>
            </div>
            </div>--%>
    <div id="ConfmingFlight" class="CfltFare" style="display: none;">
        <div id="divLoadcf" class="">
        </div>
    </div>


     <div class="one-way-select hide">

        <div class="col-md-12" id="hdvhgvhgvfh">
        </div>

    </div>

    <%--   </div>--%>
    <div class="clear"></div>
    <a href="#toptop"><span class="toptop" style="position: fixed; bottom: 4px; right: 20px; height: 50px; font-size: 20px; width: 50px; border-radius: 50%; cursor: pointer; padding: 13px 15px; background: rgb(0, 75, 145); color: rgb(255, 255, 255); display: block;"><i class="fa fa-chevron-up" aria-hidden="true"></i></span></a>
    <div class="clear1"></div>
    <input type="hidden" id="hdnMailString" name="hdnMailString" />
    <input type="hidden" id="hdnAllOrSelecte" name="hdnAllOrSelecte" />
    <input type="hidden" id="hdnOnewayOrRound" name="hdnOnewayOrRound" />
    <asp:Literal ID="henAgcDetails" runat="server" Visible="false"></asp:Literal>


    <script type="text/javascript">
        $(document).ready(function () {

            function high() {
                //$(".theme-hero-area-body").css("height", "80px");
            }
            window.onload = high;
            $("#show-hide").click(function () {

                var k = "";
                if (k == "") {
                    $(".re-search").slideToggle();
                    $(".theme-hero-area-body").css("height", "100%");
                    var k = 1;
                }
            });
            $("#show-hide-mob").click(function () {

                var k = "";
                if (k == "") {
                    $(".re-search").slideToggle();
                    $(".theme-hero-area-body").css("height", "100%");
                    var k = 1;
                }
            });

            $("#hide").click(function () {

                var k = "";
                if (k == "") {
                    $(".re-search").hide();
                    //$(".theme-hero-area-body").css("height", "100%");
                    var k = 1;
                }
            });


           

        });
    </script>

      <script type="text/javascript">
          (function () {
              var loaderText = document.getElementById("loading-msg");
              var refreshIntervalId = setInterval(function () {
                  loaderText.innerHTML = getLoadingText();
              }, 2500);

              function getLoadingText() {
                  var strLoadingText;
                  var arrLoadingText = ["Please Wait", "While we are fetching best fare for you"
                  ];
                  var rand = Math.floor(Math.random() * arrLoadingText.length);
                  return arrLoadingText[rand];
              }
          })();
      </script>


    

<%--    <script type="text/javascript">
        $(document).ready(function () {

            function high() {
                //$(".theme-hero-area-body").css("height", "80px");
            }
            window.onload = high;
            $("#show-hide").click(function () {
                debugger;
                var k = "";
                if (k == "") {
                    $(".re-search").slideToggle();
                    $(".theme-hero-area-body").css("height", "100%");
                    var k = 1;
                }
            });
        });
    </script>--%>

    <script type="text/javascript">
        var UrlBase = '<%=ResolveUrl("~/")%>';
    </script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-1.7.1.min.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-ui-1.8.8.custom.min.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/json2.js") %>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/Flight/JSLINQ.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jplist.min.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/Flight/SortAD.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/Flight/TextFilterGroup.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/Flight/handleQueryString.js?v=4.9")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/Flight/jquery.blockUI.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/Flight/async.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/Flight/LZCompression.js?y=1.8")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/Flight/FlightResultsNew.js?v=1.6")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery.tooltip.min.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/gridview-readonly-script.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/Flight/FlightMailing.js") %>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/Flight/pako.min.js") %>"></script>
    <%--   <script src="../Scripts/script.js" type="text/javascript"></script>--%>
    <script type="text/javascript">
        $(document).ready(function () {
            $(".abc1").unbind('click').bind('click', function (e) {

                if (e.target.id != "CheckboxT1") {
                    $(this).toggleClass("bdrss");
                }
            });
            $(".abc2t").unbind('click').bind('click', function (e) {

                if (e.target.id != "CheckboxT2") {
                    $(this).toggleClass("bdrss");
                }
            });
            $(".abc3t").unbind('click').bind('click', function (e) {

                if (e.target.id != "CheckboxT3") {
                    $(this).toggleClass("bdrss");
                }

            });
            $(".abc4t").unbind('click').bind('click', function (e) {

                if (e.target.id != "CheckboxT4") {
                    $(this).toggleClass("bdrss");
                }

            });

        });
    </script>
    <script type="text/javascript">
        jQuery("document").ready(function ($) {
            debugger;
            var nav = $('.nav-container');
            $(window).scroll(function () {
                if ($(this).scrollTop() > 175) {
                    $(".toptop").fadeIn();
                    if ($("#lftdv1").is(":visible") == false) {
                        nav.addClass("f-nav1");
                    }
                    else {
                        nav.addClass("f-nav");
                    }
                } else {
                    $(".toptop").fadeOut();
                    nav.removeClass("f-nav");
                    nav.removeClass("f-nav1");
                }
            });



        });
        //$(document).ready(function () {
        //    debugger;
        //    var vars = [], hash;
        //    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
        //    for (var i = 0; i < hashes.length; i++) {
        //        hash = hashes[i].split('=');
        //        vars.push(hash[0]);
        //        vars[hash[0]] = hash[1];
        //    }
        //    if (vars.TripType != "rdbOneWay") {
        //        $('.fltkun').hide();
        //    }
        //});
        //document.onreadystatechange = function () {
        //    debugger;
        //    if (document.readyState === "complete") {
        //        myFunction();
        //    }
        //    else {
        //        window.onload = function () {
        //            myFunction();
        //        };
        //    };
        //}


    </script>


    <script type="text/javascript">

                                         //jQuery("document").ready(function ($) {
                                         //    debugger;

                                         //    $(".onewayss").removeClass("col-md-4 nopad text-search mltcs").addClass("col-md-5 nopad text-search mltcs");

                                         //    $("#rdbOneWay").click(function () {

                                         //        $(".onewayss").removeClass("col-md-4 nopad text-search mltcs").addClass("col-md-5 nopad text-search mltcs");
                                         //        $(".sk").removeAttr(disabled = "disabled");
                                         //    });

                                         //    $("#rdbRoundTrip").click(function () {

                                         //        $(".onewayss").removeClass("col-md-5 nopad text-search mltcs").addClass("col-md-4 nopad text-search mltcs");

                                         //    });
                                         //    $("#rdbMultiCity").click(function () {
                                         //        $(".onewayss").removeClass("col-md-4 nopad text-search mltcs").addClass("col-md-5 nopad text-search mltcs");
                                         //    });

                                         //    var nav = $('.nav-container');
                                         //    $(window).scroll(function () {
                                         //        if ($(this).scrollTop() > 175) {
                                         //            $(".toptop").fadeIn();
                                         //            if ($("#lftdv1").is(":visible") == false) {
                                         //                nav.addClass("f-nav1");
                                         //            }
                                         //            else {
                                         //                nav.addClass("f-nav");
                                         //            }
                                         //        } else {
                                         //            $(".toptop").fadeOut();
                                         //            nav.removeClass("f-nav");
                                         //            nav.removeClass("f-nav1");
                                         //        }
                                         //    });

                                         //    var Triptype = getUrlVars()["TripType"];
                                         //    if (Triptype == "rdbOneWay") {
                                         //        $(".onewayss").removeClass("col-md-4 nopad text-search mltcs").addClass("col-md-5 nopad text-search mltcs");
                                         //    }



                                         //    //if (Triptype == "rdbOneWay") {
                                         //    //    $(".onewayss").removeAttr(disable = "disable");
                                         //    //}

                                         //    //if (Triptype == "rdbRoundTrip") {
                                         //    //    $(".onewayss").removeAttr(disable = "disable");
                                         //    //}


                                         //    if (Triptype == "rdbRoundTrip") {
                                         //        $(".onewayss").removeClass("col-md-5 nopad text-search mltcs").addClass("col-md-4 nopad text-search mltcs");
                                         //    }
                                         //});
                                         function getUrlVars() {
                                             var vars = [], hash;
                                             var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
                                             for (var i = 0; i < hashes.length; i++) {
                                                 hash = hashes[i].split('=');
                                                 vars.push(hash[0]);
                                                 vars[hash[0]] = hash[1];
                                             }
                                             return vars;
                                         }
    </script>

</asp:Content>
