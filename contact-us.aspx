﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage_Test.master" AutoEventWireup="false" CodeFile="contact-us.aspx.vb" Inherits="contact_us" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
   
  <div class="theme-hero-area theme-hero-area-half">
        <div class="theme-hero-area-bg-wrap">
        
            <div class="theme-hero-area-bg" style="background-image: url(Advance_CSS/img/d3nh53dgvze_1500x800.jpg);"></div>
            <div class="theme-hero-area-mask theme-hero-area-mask-half"></div>
            <div class="theme-hero-area-inner-shadow"></div>
        </div>
        <div class="theme-hero-area-body">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2 theme-page-header-abs">
                        <div class="theme-page-header theme-page-header-lg">
                            <h1 class="theme-page-header-title">Contact Details</h1>
                            <p class="theme-page-header-subtitle">World's leading tour and travels Booking website. Book flight tickets and enjoy your holidays with distinctive experience</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <section>
		<div class="form form-spac rows con-page">
			<div class="container">
				<!-- TITLE & DESCRIPTION -->
			

		<div class="pg-contact">
                     <%--   <div class="col-md-3 col-sm-6 col-xs-12 new-con new-con1">
                            <h2>Faretripbox</h2>
                            <p>Faretripbox is more than just a website or company. Faretripbox is for belief of agents that every agents has for their travelers, to distinctive experience of their travel and to grow.</p>
                        </div>--%>
                        <div class="col-md-4 col-sm-6 col-xs-12 new-con new-con1"> <img src="img/contact/1.html" alt="">
                            <h4>Address</h4>
                            <p>"FARETRIPBOX.COM"</p>
                            <p>Unit-1 , First Floor, NM1 Building, Sector 14 ,</p>
                            <p>Gurgaon - 122001</p>
                            <p>Haryana, India</p>
                        </div>
                        <div class="col-md-4 col-sm-6 col-xs-12 new-con new-con3"> <img src="img/contact/2.html" alt="">
                            <h4>CONTACT INFO:</h4>
                            <p>Helipline</p>
                            <p class="row">
                                <span class="col-md-6">
                                    Open All Days
                               </span>
                                <span class="col-md-6">
                                    0124-4255544
                                </span>

                            </p>

                            <p>Mail Support</p>
                            <p>
                                <span>
                                    Support : support@faretripbox.com
                                </span>
                            </p>
                            <p>
                                <span>Sales : support@faretripbox.com</span>
                            </p>
                        </div>
                        <div class="col-md-4 col-sm-6 col-xs-12 new-con new-con4"> <img src="img/contact/3.html" alt="">
                            <h4>Website</h4>
                            <p> <a href="#">Website: www.faretripbox.in</a>
                            
                                </p>
                        </div>
                    </div>				
			</div>
		</div>
	</section>
    
      
    

</asp:Content>

