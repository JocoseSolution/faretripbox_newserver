﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


public partial class Reports_Accounts_OldProtalLedger : System.Web.UI.Page   
    {
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myAmdDB"].ToString());
        string CS = ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["UID"] == null || string.IsNullOrEmpty(Convert.ToString(Session["UID"])))
            {
               Response.Redirect("~/Login.aspx");
            } 
        }

        protected void BtnSearch_Click(object sender, EventArgs e)
        {
           DataTable FltOldPortalList = new DataTable();
            FltOldPortalList = SelectOldPortalID(Session["UID"].ToString());            
            string FromDate = null;
            string ToDate = null;
            string AgencyName = FltOldPortalList.Rows[0]["OldPortalId"].ToString();

            string PNR = "";			
			PNR=txt_PNR.Text;

            if (String.IsNullOrEmpty(Request["From"]))
            {
                FromDate = "";
            }
            else
            {
                if (Request["From"].Split('-').Length > 2)
                {
                    FromDate = Request["From"].Split('-')[2] + "-" + Request["From"].Split('-')[1] + "-" + Request["From"].Split('-')[0];
                    FromDate = FromDate + " " + "12:00:00 AM";
                }
                else
                {
                    FromDate = "";
                }
            }            
            if (String.IsNullOrEmpty(Request["To"]))
            {
                ToDate = "";
            }
            else
            {
                if (Request["To"].Split('-').Length > 2)
                {
                    ToDate = Request["To"].Split('-')[2] + "-" + Request["To"].Split('-')[1] + "-" + Request["To"].Split('-')[0];
                    ToDate = ToDate + " " + "11:59:59 PM";
                }
                else
                {
                    ToDate = "";
                }
            }
                String sp = "GetDataAccount__2021011";
                con.Open();
                SqlCommand cmd = new SqlCommand(sp, con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@FormDate", FromDate);
                cmd.Parameters.AddWithValue("@ToDate", ToDate);
				cmd.Parameters.AddWithValue("@AgentId", AgencyName);
				cmd.Parameters.AddWithValue("@pnr", PNR);
                SqlDataAdapter sd = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                sd.Fill(dt);
                cmd.ExecuteNonQuery();
                ticket_grdview.DataSource = dt;
                ticket_grdview.DataBind();
                con.Close();
           

        }
    public DataTable SelectOldPortalID(string AgencyID)
    {
        SqlDataAdapter adap = new SqlDataAdapter();      
        
            DataTable dt = new DataTable();

            adap = new SqlDataAdapter("Select OldPortalId from [dbo].[OldPortalLedger]  where AgencyId = '" + AgencyID + "' ", con);            
            adap.Fill(dt);           
        
        return dt;
    }

}
