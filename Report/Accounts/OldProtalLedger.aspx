﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="OldProtalLedger.aspx.cs" Inherits="Reports_Accounts_OldProtalLedger" MasterPageFile="~/MasterPageForDash.master" Title="" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <%--<link href="../../CSS/style.css" rel="stylesheet" type="text/css" />
    <link href="../../css/main2.css" rel="stylesheet" type="text/css" />
    <link href="../../CSS/StyleSheet.css" rel="stylesheet" type="text/css" />--%>

    <link type="text/css" href="<%=ResolveUrl("~/Styles/jquery-ui-1.8.8.custom.css") %>"
        rel="stylesheet" />
    <link type="text/css" href="<%=ResolveUrl("~/CSS/newcss/main.css")%>"
        rel="stylesheet" />
    <style type="text/css">
        .HeaderStyle th {
            white-space: nowrap;
        }

        .ndsmsg {
            color: red !important;
        }

        .popupnew2 {
            position: absolute;
            top: 650px;
            left: 7%;
            width: 900PX;
            height: 500px !important;
            z-index: 1;
            box-shadow: 0px 5px 5px #f3f3f3;
            border: 2px solid #004b91;
            background-color: #fff;
            background-color: #ffffff !important;
            padding: 10px 20px;
            overflow-x: hidden;
        }

        .lft {
            float: left;
        }

        .rgt {
            float: right;
        }

        .fade {
            opacity: 1;
            -webkit-transition: opacity .15s linear;
            -o-transition: opacity .15s linear;
            transition: opacity .15s linear;
        }

        .vew321 {
            background-color: #fff;
            width: 75%;
            float: right;
            padding: 5px 10px;
            text-align: justify;
            height: 300px;
            overflow-x: scroll !important;
            overflow-y: scroll !important;
            z-index: 1;
            position: fixed;
            top: 100px;
            left: 20%;
            border: 5px solid #d1d1d1;
        }
    </style>
    <style type="text/css">
        .page-wrapperss {
            background-color: #fff;
            margin-left: 15px;
        }

        .overfl {
            overflow: auto;
        }
        .marginbtn{
            margin-top: 30px;
        }
        .form-group > label {
            color: #000;
        }
    </style>
    <script type="text/javascript">
        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode;
            if (charCode >= 48 && charCode <= 57 || charCode == 08) {
                return true;
            }
            else {
                return false;
            }
        }
    </script>
    <div class="card-main">
        <div class="card-header">
            <h3 class="main-heading">Old Portal Ledger Details</h3>
        </div>
        <div class="card-body">
            <div class="inner-box">
                <div class="row">
                    <div class="col-sm-12">
                        <h3 class="panel-title">Old Ledger Report</h3>
                    </div>
                </div>
        
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                         <label>From </label>
                       <input type="text" name="From" id="From" placeholder="From Date" class="form-control" readonly="readonly" />
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                         <label>To</label>
                        <input type="text" name="To" placeholder="To Date" id="To" class="form-control" readonly="readonly" />
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label>Airline PNR</label>
                        <asp:TextBox ID="txt_PNR" runat="server"  placeholder="Pnr.." CssClass="form-control"></asp:TextBox>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <div class="w20 lft">&nbsp;</div>
                        <asp:Button ID="BtnSearch" runat="server" CssClass="btn cmn-btn marginbtn" Text="Search Result" OnClick="BtnSearch_Click" />
                    </div>
                </div>
            </div>
                    </div>
            <hr />
            <br />
            <div class="table-responsive">
                <asp:GridView ID="ticket_grdview"  runat="server" AllowPaging="True" AutoGenerateColumns="False"
                    CssClass="rtable" GridLines="Both" Font-Size="12px" PageSize="30">
                    <Columns>
                        <asp:BoundField HeaderText="Payment Time" DataField="Payment_Time"></asp:BoundField>
                        <asp:BoundField HeaderText="Reseller Id" DataField="Reseller_Id"></asp:BoundField>
                        <asp:BoundField HeaderText="Reseller Name" DataField="Reseller_Name"></asp:BoundField>
                        <asp:BoundField HeaderText="Reason" DataField="Reason"></asp:BoundField>
                        <asp:BoundField HeaderText="Payment Medium" DataField="Payment_Medium"></asp:BoundField>
                        <asp:BoundField HeaderText="Operation" DataField="Operation"></asp:BoundField>
                        <asp:BoundField HeaderText="Cart_ID" DataField="Cart_ID"></asp:BoundField>
                        <asp:BoundField HeaderText="Pay_Amount" DataField="Pay_Amount"></asp:BoundField>
                        <asp:BoundField HeaderText="Payment_Fee" DataField="Payment_Fee"></asp:BoundField>
                        <asp:BoundField HeaderText="Net Amount" DataField="Net_Amount"></asp:BoundField>
                        <asp:BoundField HeaderText="Deposit" DataField="Deposit"></asp:BoundField>
                        <asp:BoundField HeaderText="Reseller Accounts Code" DataField="Reseller_Accounts_Code"></asp:BoundField>
                        <asp:BoundField HeaderText="TxnId" DataField="TxnId"></asp:BoundField>
                        <asp:BoundField HeaderText="Deposit_Limit" DataField="Deposit_Limit"></asp:BoundField>
                        <asp:BoundField HeaderText="Passenger names" DataField="Passenger_names"></asp:BoundField>
                        <asp:BoundField HeaderText="PNR" DataField="GDS_Ticket_number/LCC_Airline_PNR"></asp:BoundField>
                        <asp:BoundField HeaderText="Airline Code" DataField="Airline_name/Code"></asp:BoundField>

                    </Columns>

                </asp:GridView>
            </div>
        </div>
    </div> 
     <script type="text/javascript">
         var UrlBase = '<%=ResolveUrl("~/") %>';
     </script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-1.4.4.min.js") %>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-ui-1.8.8.custom.min.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/AgencySearch.js") %>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/JS/Distributor.js") %>"></script>
    <style type="text/css">
        .bdrbtm1 {
            border-bottom: 2px solid #ddd;
        }
    </style>
</asp:Content>
