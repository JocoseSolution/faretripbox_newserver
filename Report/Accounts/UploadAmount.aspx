﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageForDash.master" AutoEventWireup="true" CodeFile="UploadAmount.aspx.cs" Inherits="SprReports_Accounts_UploadAmount" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <link type="text/css" href="<%=ResolveUrl("~/Styles/jquery-ui-1.8.8.custom.css") %>"
        rel="stylesheet" />

    <style>
       

        table {
            /*width: 750px;*/
            border-collapse: collapse;
            /*margin:50px auto;*/
        }

        tr:nth-of-type(odd) {
            background: #eee;
        }

        th {
            background: #0952a4;
            color: white;
            /*font-weight: bold;*/
        }

        td, th {
            padding: 2px;
            border: 1px solid #ccc;
            text-align: left;
            /*font-size: 18px;*/
        }


        @media only screen and (max-width: 760px), (min-device-width: 768px) and (max-device-width: 1024px) {

            table {
                width: 100%;
            }


            table, thead, tbody, th, td, tr {
                display: block;
                padding: inherit;
            }


                thead tr {
                    position: absolute;
                    top: -9999px;
                    left: -9999px;
                }

            tr {
                border: 1px solid #ccc;
            }

            td {
                border: none;
                border-bottom: 1px solid #eee;
                position: relative;
                padding-left: 50%;
            }

                td:before {
                    position: absolute;
                    top: 6px;
                    left: 6px;
                    width: 45%;
                    padding-right: 10px;
                    white-space: nowrap;
                    content: attr(data-column);
                    color: #000;
                    font-weight: bold;
                }
        }
    </style>

    <style>
        .content {
            height: 200px;
        }

        a {
            text-decoration: none;
        }


        .container {
            max-width: 1200px;
            margin: 0 auto;
            width: 100%;
        }

        .nav-fostrap {
               display: block;
            margin-bottom: 15px 0;
            background: #ffffff;
            box-shadow: 0px 1px 10px 1px rgba(0, 0, 0, 0.12);
            line-height: 0px;
            z-index: 999;
            margin-top: 100px;
        }

            .nav-fostrap ul {
                list-style-type: none;
                margin: 0;
                padding: 0;
                display: block;
            }

            .nav-fostrap li {
                list-style-type: none;
                margin: 0;
                padding: 0;
                display: inline-block;
                position: relative;
                font-size: 14;
                color: #def1f0;                
                border-right: 1px solid #e0e0e0;
            }

                .nav-fostrap li a {
                     padding: 10px 10px;
                    font-size: 14px !important;
                    color: #7a7a7a;
                    display: inline-block;
                    outline: 0;
                    font-weight: 500;
                }
                .nav-fostrap li:last-child {
                       border-right: none;
                }

                .nav-fostrap li:hover ul.dropdown {
                    display: block;
                }

                .nav-fostrap li ul.dropdown {
                    position: absolute;
                    display: none;
                    width: 200px;
                    background: #2e323e;
                    -webkit-box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.26);
                    -moz-box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.26);
                    -ms-box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.26);
                    -o-box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.26);
                    box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.26);
                    padding-top: 0;
                    left: -105px;
                       z-index: 999;
                }

                    .nav-fostrap li ul.dropdown li {
                        display: block;
                        list-style-type: none;
                    }

                        .nav-fostrap li ul.dropdown li a {
                            padding: 15px 20px;
                            font-size: 15px;
                            color: #fff;
                            display: block;
                            font-weight: 400;
                        }

                        .nav-fostrap li ul.dropdown li:last-child a {
                            border-bottom: none;
                        }

                .nav-fostrap li:hover a {
                    background: #2e323e;
                    color: #fff !important;
                }

                .nav-fostrap li:first-child:hover a {
                    border-radius: 3px 0 0 3px;
                }

                .nav-fostrap li ul.dropdown li:hover a {
                    background: rgba(0,0,0, .1);
                }

                .nav-fostrap li ul.dropdown li:first-child:hover a {
                    border-radius: 0;
                }

                .nav-fostrap li:hover .arrow-down {
                    border-top: 5px solid #fff;
                }

        .arrow-down {
            width: 0;
            height: 0;
            border-left: 5px solid transparent;
            border-right: 5px solid transparent;
            border-top: 5px solid #def1f0;
            position: relative;
            top: 15px;
            right: -5px;
            content: '';
        }

        .title-mobile {
            display: none;
        }

        @media only screen and (max-width:900px) {

            .nav-fostrap {
                background: #fff;
                width: 200px;
                height: 100%;
                display: block;
                position: fixed;
                left: -200px;
                top: 0px;
                -webkit-transition: left 0.25s ease;
                -moz-transition: left 0.25s ease;
                -ms-transition: left 0.25s ease;
                -o-transition: left 0.25s ease;
                transition: left 0.25s ease;
                margin: 0;
                border: 0;
                border-radius: 0;
                overflow-y: auto;
                overflow-x: hidden;
                height: 100%;
            }

            .title-mobile {
                position: fixed;
                display: block;
                top: 10px;
                font-size: 20px;
                left: 100px;
                right: 100px;
                text-align: center;
                color: #FFF;
            }

            .nav-fostrap.visible {
                left: 0px;
                -webkit-transition: left 0.25s ease;
                -moz-transition: left 0.25s ease;
                -ms-transition: left 0.25s ease;
                -o-transition: left 0.25s ease;
                transition: left 0.25s ease;
            }

            .nav-bg-fostrap {
                display: inline-block;
                vertical-align: middle;
                width: 100%;
                height: 50px;
                margin: 0;
                position: absolute;
                top: 0px;
                left: 0px;
                background:  #f86811
                padding: 12px 0 0 10px;
                -webkit-box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.26);
                -moz-box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.26);
                -ms-box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.26);
                -o-box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.26);
                box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.26);
            }

            .navbar-fostrap {
                display: inline-block;
                vertical-align: middle;
                height: 50px;
                cursor: pointer;
                margin: 0;
                position: absolute;
                top: 0;
                left: 0;
                padding: 12px;
            }

                .navbar-fostrap span {
                    height: 2px;
                    background: #fff;
                    margin: 5px;
                    display: block;
                    width: 20px;
                }

                    .navbar-fostrap span:nth-child(2) {
                        width: 20px;
                    }

                    .navbar-fostrap span:nth-child(3) {
                        width: 20px;
                    }


                    nav ul a:hover {
    background-color: rgb(222, 222, 222) !important;
}


            .nav-fostrap ul {
                padding-top: 50px;
            }

            .nav-fostrap li {
                display: block;
            }

                .nav-fostrap li a {
                    display: block;
                    color: #505050;
                    font-weight: 600;
                }

                .nav-fostrap li:first-child:hover a {
                    border-radius: 0;
                }

                .nav-fostrap li ul.dropdown {
                    position: relative;
                }

                    .nav-fostrap li ul.dropdown li a {
                        background: #2980B9 !important;
                        border-bottom: none;
                        color: #fff !important;
                    }

                .nav-fostrap li:hover a {
                    background: #03A9F4;
                    color: #fff !important;
                }

                .nav-fostrap li ul.dropdown li:hover a {
                    background: rgba(0,0,0,.1);
                    !important;
                    color: #fff !important;
                }

                .nav-fostrap li ul.dropdown li a {
                    padding: 10px 10px 10px 30px;
                }

                .nav-fostrap li:hover .arrow-down {
                    border-top: 5px solid #fff;
                }

            .arrow-down {
                border-top: 5px solid #505050;
                position: absolute;
                top: 20px;
                right: 10px;
            }

            .cover-bg {
                background: rgba(0,0,0,0.5);
                position: fixed;
                top: 0;
                bottom: 0;
                left: 0;
                right: 0;
            }
        }

        @media only screen and (max-width:1199px) {

            .container {
                width: 96%;
            }
        }

        .fixed-top {
            position: fixed;
            top: 0;
            right: 0;
            left: 0;
        }
    </style>

    <script>

        $(document).ready(function () {
            $('.navbar-fostrap').click(function () {
                $('.nav-fostrap').toggleClass('visible');
                $('body').toggleClass('cover-bg');
            });
        });
    </script>
    <%--<link type="text/css" href="<%=ResolveUrl("~/CSS/newcss/main.css")%>"
        rel="stylesheet" />--%>
    <%--<style>
        .msi {
            width: 130% !important;
            max-width: 130% !important;
        }
        #ctl00_ContentPlaceHolder1_rblPaymentMode {
            margin:0 auto;
        }
    </style>--%>

    <nav style="z-index:1011;">
        <div class="nav-fostrap">
            <ul>
                <li><a href="<%= ResolveUrl("~/Report/Agent/UploadAmountDetails.aspx")%>">Upload Status</a></li> 
                                <li><a href="<%= ResolveUrl("~/Report/Accounts/CashInFlow.aspx")%>">CashInFlow Report</a></li>
                                <li><a href="<%= ResolveUrl("~/Report/Accounts/UploadAmount.aspx")%>">Wallet Top-Up</a></li>
                                <li><a href="<%= ResolveUrl("~/Report/Distr/UploadRequest.aspx")%>">Agent Upload Request</a></li>
                                <li><a href="<%= ResolveUrl("~/Report/Distr/InProcessUploadDetails.aspx")%>">InProcess Upload</a></li>
                                <%--<li><a href="<%= ResolveUrl("~/Report/Distr/RegisterAgent.aspx")%>">Add Agent</a></li>--%>
                                <li><a href="<%= ResolveUrl("~/Report/Admin/Agent_Details.aspx")%>">Agent Details</a></li>
                                <li><a href="<%= ResolveUrl("~/Report/Distr/AddAgent.aspx")%>">Add Agent</a></li>
                                 
                                
                    
        
            <li><a href="javascript:void(0)">More<span class="arrow-down"></span></a>
                <ul class="dropdown">
                    <li><a href="<%= ResolveUrl("~/Report/Agent/AgentTypeDetails.aspx")%>">Create GroupType</a></li>
                    <li><a href="<%= ResolveUrl("~/Report/Admin/CommissionMaster.aspx")%>">CommissionMaster</a></li>
                                <li><a href="<%= ResolveUrl("~/Report/Agent/agent_markup.aspx")%>">Set Agent Markup</a></li>
                                <li><a href="<%= ResolveUrl("~/Report/Agent/TypeDetailsRefresh.aspx")%>">Refresh New GroupType</a></li>
                                <li><a href="<%= ResolveUrl("~/Report/Distr/DepositeOffice.aspx")%>">Deposite Office</a></li>
                                <li><a href="<%= ResolveUrl("~/Report/Distr/AddBankDetails.aspx")%>">Add Bank Details</a></li>
                </ul>
            </li>
            </ul>
        </div>
        <div class="nav-bg-fostrap">
            <div class="navbar-fostrap"><span></span><span></span><span></span></div>
            <a href="" class="title-mobile">Fostrap</a>
        </div>
    </nav>





    <div class="card-main">

      <div class="card-header">
                <h3  class="main-heading">Wallet Top-Up By Payment Gateway</h3>
          </div>
      
        <div class="inner-box wallet">
            <div class="col-md-12">
              
                    <div class="row">
                        <div class="col-md-4">
                            <label>Enter Amount</label>
                            <asp:TextBox ID="TxtAmout" runat="server" class="form-c ontrolaa" onkeypress="return keyRestrict(event,'0123456789');" MaxLength="11" Text="0" AutoCompleteType="Disabled" style="color:#000;"></asp:TextBox>
                         </div>
                        <div class="col-md-4 mt-3">
                          <b>Transaction Charges:</b> &nbsp; &nbsp;<asp:Label ID="lblTransCharges" runat="server"></asp:Label>
                        </div>   
                        <div class="col-md-4 mt-3">
                            <b>Total  Amount:</b>&nbsp; &nbsp;<asp:Label ID="lblTotalAmount" runat="server"></asp:Label>
                        </div>
                     </div>
                </div>
                     <div class="wallet">
                                <div class="col-md-12">       
                                    
                                 <h2 class="sub-heading">Payment Mode</h2>
                                    <div class="row">
                                            <div class="col-md-2">                     

                                        <asp:RadioButtonList ID="rblPaymentMode" runat="server" RepeatDirection="Vertical">
                                        <asp:ListItem Text="Credit Card" Value="razorpaycredit" Selected="True"></asp:ListItem>
			                <asp:ListItem Text="Debit card" Value="razorpaydebit"></asp:ListItem>
                                        <asp:ListItem Text="Net Banking" Value="razorpaynetbanking"></asp:ListItem>
                                        <asp:ListItem Text="UPI" Value="razorpayupi"></asp:ListItem>
                                        <asp:ListItem Text="Wallet" Value="razorpaywallet"></asp:ListItem>
                                                 <%--
                                                     <asp:ListItem Text="Razor" Value="Razor" Selected="True">&nbsp;<img src="../../Images/icons/credit_card_PNG107.png" title="Razor" style="width: 42px;"/> &nbsp;Razor</asp:ListItem>
                                        <asp:ListItem Text="Debit card Razorpay" Value="razorpaydebit"></asp:ListItem>
                                        <asp:ListItem Text="Net Banking Razorpay" Value="razorpaynetbanking"></asp:ListItem>
                                        <asp:ListItem Text="UPI Razorpay" Value="razorpayupi"></asp:ListItem>
                                        <asp:ListItem Text="Wallet Razorpay" Value="razorpaywallet"></asp:ListItem>
                                        <asp:ListItem Text="EMI Razorpay" Value="razorpayemi"></asp:ListItem>--%>  
                                                
                                                
                                                <%--  <asp:ListItem Text="Credit Card" Value="creditcard">&nbsp;<img src="../../Images/icons/credit_card_PNG107.png" title="Credit Card" style="width: 42px;"/></asp:ListItem>
                                                <asp:ListItem Text="Debit Card" Value="debitcard">&nbsp;<img src="../../Images/icons/Debit.png" title="Debit Card" style="width: 42px;"/></asp:ListItem>
                                                <asp:ListItem Text="Net Banking" Value="netbanking">&nbsp;<img src="../../Images/icons/net-banking-png.png" title="Net Banking" style="width: 42px;"/></asp:ListItem>
                                                <asp:ListItem Text="UPI" Value="upi">&nbsp;<img src="../../Images/icons/UPI.png" title="UPI" style="width: 42px;"/></asp:ListItem>
                                                <asp:ListItem Text="Wallets" Value="cashcard">&nbsp;<i class="fa fa-user"></i></asp:ListItem>
                                                <asp:ListItem Text="Paytm" Value="Paytm">&nbsp;<i class="fa fa-user"></i></asp:ListItem>
                                                <asp:ListItem Text="FreeCharge" Value="Freecharge">&nbsp;<i class="fa fa-user"></i></asp:ListItem>
                            
                                                <asp:ListItem Text="MobiKwik" Value="Mobikwik"></asp:ListItem>--%>
                                            </asp:RadioButtonList>
                                        </div>
                                            <div class="col-md-2">
                                    <asp:Label ID="lblerrormsg" runat="server"></asp:Label>

                                </div>
                                       <div class="col-md-12">
                                                    
                                            <div class="btn-upload">
                                                 <asp:Button ID="BtnUpload" runat="server" CssClass="btn cmn-btn" Text="Upload Amount" OnClick="BtnUpload_Click"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
            </div>

          
        </div>


              
              

                    
                    <%-- <div class="col-md-4  text-center ">
                </div>--%>
             
    <br />
    <br />
    <br />
    <br />



        <input type="hidden" id="TransCharges" name="TransCharges" />
        <input type="hidden" id="TotalAmount" name="TotalAmount" />
 
    

    <script type="text/javascript">
        var UrlBase = '<%=ResolveUrl("~/") %>';

        function preventBack() { window.history.forward(); }
        setTimeout("preventBack()", 0);
        window.onunload = function () { null };
    </script>

    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-1.4.4.min.js") %>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-ui-1.8.8.custom.min.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/AgencySearch.js") %>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/JS/Distributor.js") %>"></script>

    <%-- <script type="text/javascript">        
        $(document).ready(function () {
            $("#ctl00_ContentPlaceHolder1_TxtAmout").keypress(function () {
                GetPgTransCharge();
            });
        });
</script>--%>

    <script type="text/javascript">
        //function preventBack() { window.history.forward(); }
        //setTimeout("preventBack()", 0);
        //window.onunload = function () { null };
        //window.onload = function () {
        //    document.getElementById("ctl00_ContentPlaceHolder1_TxtAmout").name = "txt" + Math.random();
        //}
    </script>

    <script type="text/javascript">
        function ValidateAmount() {
            if ($("#ctl00_ContentPlaceHolder1_TxtAmout").val().trim() == "") {
                alert("Plese enter upload amount.")
                return false;
            }
            if (parseFloat($("#ctl00_ContentPlaceHolder1_TxtAmout").val()) < 1) {
                alert("Plese enter amount greater than zero.")
                return false;
            }
            GetPgTransCharge();

            var bConfirm = confirm('Payment gateway transaction charges Rs. ' + $('#TransCharges').val() + ' and  Total Amount Rs. ' + $('#TotalAmount').val() + ' debit from your bank a/c, Are you sure upload amount?');
            if (bConfirm == true) {
                $("#ctl00_ContentPlaceHolder1_BtnUpload").hide();
                return true;
            }
            else {
                $("#ctl00_ContentPlaceHolder1_BtnUpload").show();
                return false;
            }
        }

        $("#ctl00_ContentPlaceHolder1_TxtAmout").keyup(function () {
            GetPgTransCharge();
            //if ($("#ctl00_ContentPlaceHolder1_TxtAmout").val().trim() == "" && $("#ctl00_ContentPlaceHolder1_TxtAmout").val() == null) {
            //    var str = 0;
            //    $("#ctl00_ContentPlaceHolder1_TxtAmout").val(str);
            //}            
        })

        $("#ctl00_ContentPlaceHolder1_rblPaymentMode").click(function () {
            GetPgTransCharge();
        });
        function GetPgTransCharge() {
            var checked_radio = $("[id*=ctl00_ContentPlaceHolder1_rblPaymentMode] input:checked");
            var PaymentMode = checked_radio.val();
            var TotalPgCharges = 0;
            var TotalAmount = 0;
            var Amount = 0;
            if ($("#ctl00_ContentPlaceHolder1_TxtAmout").val().trim() != "" && $("#ctl00_ContentPlaceHolder1_TxtAmout").val() != null) {
                Amount = $("#ctl00_ContentPlaceHolder1_TxtAmout").val();
            }
            else {
                Amount = 0
            }
            $.ajax({
                type: "POST",
                url: "UploadAmount.aspx/GetPgChargeByMode",
                data: '{paymode: "' + PaymentMode + '" }',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    if (data.d != "") {
                        if (data.d.indexOf("~") > 0) {
                            //var res = data.d.split('~');
                            var PgCharge = data.d.split('~')[0]
                            var chargeType = data.d.split('~')[1]
                            if (chargeType == "F") {
                                //calculate fixed pg charge 
                                TotalPgCharges = (parseFloat(PgCharge)).toFixed(2);
                                TotalAmount = (parseFloat(Amount) + parseFloat(TotalPgCharges)).toFixed(2);

                                $('#TransCharges').val(TotalPgCharges);
                                $('#TotalAmount').val(TotalAmount);
                                $('#<%=lblTransCharges.ClientID%>').html(TotalPgCharges);
                                $('#<%=lblTotalAmount.ClientID%>').html(TotalAmount);
                            }
                            else {
                                //calculate percentage pg charge                                     
                                TotalPgCharges = ((parseFloat(Amount) * parseFloat(PgCharge)) / 100).toFixed(2);
                                TotalAmount = (parseFloat(Amount) + parseFloat(TotalPgCharges)).toFixed(2);

                                $('#TransCharges').val(TotalPgCharges);
                                $('#TotalAmount').val(TotalAmount);
                                $('#<%=lblTransCharges.ClientID%>').html(TotalPgCharges);
                                     $('#<%=lblTotalAmount.ClientID%>').html(TotalAmount);
                            }
                        }
                    }
                    else {
                        alert("try again");
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    alert(textStatus);
                }
            });
        }

        function getKeyCode(e) {
            if (window.event)
                return window.event.keyCode;
            else if (e)
                return e.which;
            else
                return null;
        }
        function keyRestrict(e, validchars) {
            var key = '', keychar = '';
            key = getKeyCode(e);
            if (key == null) return true;
            keychar = String.fromCharCode(key);
            keychar = keychar.toLowerCase();
            validchars = validchars.toLowerCase();
            if (validchars.indexOf(keychar) != -1)
                return true;
            if (key == null || key == 0 || key == 8 || key == 9 || key == 13 || key == 27)
                return true;
            return false;
        }
    </script>

</asp:Content>

