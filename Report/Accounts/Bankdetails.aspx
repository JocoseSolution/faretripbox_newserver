﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageForDash.master" AutoEventWireup="true" CodeFile="Bankdetails.aspx.cs" Inherits="SprReports_Accounts_Bankdetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    
    <br />
    <br />
    <section class="">

          <br />
    <br />
		  <br />
    <br />
        
		<div class="container">
         
			<div class="row">

					<div class="col-md-5 col-sm-5">
                    
					<div class="tourb2-ab-p4-1 tourb2-ab-p4-com hover"> <i><img src="../../Images/Bank/ICICI-Bank-PNG-Icon-715x715.png" style="width:50px;"/></i>
						<div class="tourb2-ab-p4-text">
							<h4  style=" margin-top: 0px;margin-bottom: 0px;">ICICI Bank</h4>
							<p style="font-size: 12px;"></p>
                            <p style="font-size: 12px;">ACCOUNT NO       :	354305000278<br />IFSC code        :			ICIC0002711<br />Mode : Cheque | NEFT | RTGS | IPMS<br />BRANCH :	Chhattarpur, New Delhi</p>
							<p style="background: #1fa736;width: 50%;text-align: center;border-radius: 5px;color: #fff;">Hotel Expertz</p>
						</div>
					</div>
                       
				</div>


                     <div class="col-md-5 col-sm-5">
                    
					<div class="tourb2-ab-p4-1 tourb2-ab-p4-com hover"> <i><img src="../../Images/Bank/ICICI-Bank-PNG-Icon-715x715.png" style="width:50px;"/></i>
						<div class="tourb2-ab-p4-text">
							<h4  style=" margin-top: 0px;margin-bottom: 0px;">ICICI Bank</h4>
							<p style="font-size: 12px;"></p>
                            <p style="font-size: 12px;">ACCOUNT NO       :	354305000324<br />IFSC code        :			ICIC0003543<br />Mode : Cheque | NEFT | RTGS | IPMS<br />BRANCH :	Chhattarpur, New Delhi</p>
						<p style="background: #1fa736;width: 50%;text-align: center;border-radius: 5px;color: #fff;">Faretripbox</p>
						</div>
					</div>
                       
				</div>

				<div class="col-md-5 col-sm-5">
                   
					<div class="tourb2-ab-p4-1 tourb2-ab-p4-com hover"> <i><img src="../../Images/Bank/SBILogo_state-bank-of-india-new.png" style="width:50px;"/></i>
						<div class="tourb2-ab-p4-text">
							<h4  style=" margin-top: 0px;margin-bottom: 0px;">STATE BANK OF INDIA</h4>
						
                    <p style="font-size: 12px;"></p>
                            <p style="font-size: 12px;">ACCOUNT NO       :	36527343171<br />IFSC code        :	SBIN0006837<br />Mode : Cheque | NEFT | RTGS | IPMS<br />BRANCH           :	Sultanpur, New Delhi<br /></p>
        
                          
						</div>
					</div>
                      
				</div>

             
				<div class="col-md-5 col-sm-5">                                   
					<div class="tourb2-ab-p4-1 tourb2-ab-p4-com hover"> <i><img src="../../Images/Bank/Hdfc-Logo.png"" style="width:50px;"/></i>
						<div class="tourb2-ab-p4-text">
							<h4  style=" margin-top: 0px;margin-bottom: 0px;">HDFC Bank</h4>
							<p style="font-size: 12px;"></p>
                            <p style="font-size: 12px;">ACCOUNT NO       :	50200023859055<br />IFSC code        :	HDFC0000044<br />Mode : Cheque | NEFT | RTGS | IPMS<br />BRANCH           :	Gurgaon, Haryana<br /></p>
                           
                            
						</div>
					</div>
                      
				</div>
			
			
					

            
				
			</div>
		</div>

      
     <br />
     <br />
     <br />
        

	</section>



    

</asp:Content>

