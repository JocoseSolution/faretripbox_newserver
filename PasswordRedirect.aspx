﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage_Test.master" AutoEventWireup="false" CodeFile="PasswordRedirect.aspx.vb" Inherits="PasswordRedirect" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <title>RWT</title>
    <%--<script src="Scripts/jquery-1.8.2.min.js" type="text/javascript"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js" type="text/javascript"></script>--%>
    <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7,IE=EmulateIE8,IE=EmulateIE9,IE=EmulateIE10,IE=EmulateIE11" />
    <meta http-eqiv="Content-Type" content="text/html; charset=ISO-8859-1" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" />
    <%--Like this also as below--%>
    <%--<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7,IE8,IE9,IE10,IE11" />--%>

    <script type="text/javascript">
        var UrlBase = '<%=ResolveUrl("~/")%>';
        //var ApplUrl = 'http://b2b.//.com/';
    </script>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i" rel="stylesheet" />

    <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
    <%--   New CSS and JS start--%>
<%--    <link href="<%= ResolveUrl("CSS/newcss/bootstrap.css") %>" rel="stylesheet" type="text/css" />
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" />
    <link href="CSS/newcss/main.css" rel="stylesheet" />
    <link href="CSS/itzz.css" rel="stylesheet" />

    <script type="text/javascript" src="https://use.fontawesome.com/20e527aa02.js"></script>--%>
     <link href="Advance_CSS/css/bootstrap.css" rel="stylesheet" />
    <link href="Advance_CSS/css/styles.css" rel="stylesheet" />


<%--    <link href="<%= ResolveUrl("css/itzz.css") %>" rel="stylesheet" type="text/css" />
    <link href="<%= ResolveUrl("css/app.css")%>" rel="stylesheet" type="text/css" />
    <link href="<%= ResolveUrl("css/foundation.css")%>" rel="stylesheet" type="text/css" />
    <script src="<%= ResolveUrl("~/Scripts/jquery-1.7.1.min.js")%>" type="text/javascript"></script>
    <script src="<%= ResolveUrl("~/js/lytebox.js") %>" type="text/javascript"></script>
    <script src="<%= ResolveUrl("~/JS/JScript.js") %>" type="text/javascript"></script>--%>


    <script language="javascript" type="text/javascript">
        function PWSMATCH() {
            debugger;
            var regex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[#$@!%&*?])[A-Za-z\d#$@!%&*?]{8,16}$/;
            if (!regex.test(document.getElementById("ctl00_ContentPlaceHolder1_txt_password").value)) {
                alert("Password must contain:8-16 characters atleast 1 UpperCase Alphabet, 1 LowerCase Alphabet, 1 Number and 1 Special Character'");
                document.getElementById("ctl00_ContentPlaceHolder1_txt_password").focus();
                return false;
            }

            if (!regex.test(document.getElementById("ctl00_ContentPlaceHolder1_txt_cpassword").value)) {
                alert("Password must contain:8-16 characters atleast 1 UpperCase Alphabet, 1 LowerCase Alphabet, 1 Number and 1 Special Character'");
                document.getElementById("ctl00_ContentPlaceHolder1_txt_cpassword").focus();
                return false;
            }

            if (document.getElementById("ctl00_ContentPlaceHolder1_txt_password").value != document.getElementById("ctl00_ContentPlaceHolder1_txt_cpassword").value) {
                alert('Please Enter Same Password');
                return false;
            }
        }


    </script>

 

<%--    <div style="width:100%">
        <div style="width:20%;"></div>
          
     <div style="width:80%;float:right">

         <h6>Update Password</h6>

        <div class="col-md-1 col-xs-4">Agency Name</div>
        <div class="col-md-3 col-xs-8">&nbsp;</div>
        <br />
         <br />
        
         <br />
          <div class="clear"></div>
         
    </div>
 </div>--%>
   
    <div id="logIDDiv" runat="server">Password Changed Successfully <a style="color:red" href="Login.aspx">Login</a> Here with New Password </div>

    <div class="theme-hero-area" style="margin-top:60px;">
      <div class="theme-hero-area-bg-wrap">
        <div class="theme-hero-area-bg" style="background-image:url(Advance_CSS/Images/adult-book-business-cactus-297755_1500x800.jpg);"></div>
        <div class="theme-hero-area-mask theme-hero-area-mask-strong"></div>
      </div>
      <div class="theme-hero-area-body">
        <div class="theme-page-section _pt-100 theme-page-section-xl">

             <br />
    <br />
          <div class="container">
            <div class="row">
              <div class="col-md-4 col-md-offset-4">
                <div class="theme-login theme-login-white">
                  <div class="theme-login-header">
                    <h1 class="theme-login-title">Update Password</h1>
                    <p class="theme-login-subtitle" style="color:#fff !important;">Agency Name : <asp:Label ID="txtAgencyName" runat="server" /></p>
                  </div>

                  <div class="theme-login-box">
                    <div class="theme-login-box-inner" id="td_login1"  runat="server" style="background:aliceblue;">
                     <%-- <p class="theme-login-pwd-reset-text">Enter the email associated with your account in the field below and we'll email your password.</p>--%>
              

                          <div class="form-group theme-login-form-group">
                
    
                           <asp:TextBox ID="txt_oldpassword" runat="server" TextMode="Password" CssClass="form-control" placeholder="Old Password"></asp:TextBox>
                    </div>


                        <div class="form-group theme-login-form-group">
                            <asp:TextBox ID="txt_password" MaxLength="16" runat="server" TextMode="Password" CssClass="form-control" placeholder="New Password"></asp:TextBox>
                            </div>

                        <div class="form-group theme-login-form-group">
                            <asp:TextBox ID="txt_cpassword" MaxLength="16" runat="server" TextMode="Password" CssClass="form-control" placeholder="Confirm Password"></asp:TextBox>
                
                            </div>


                   <%--   <a class="btn btn-uc btn-dark btn-block btn-lg" href="#">Reset Password</a>--%>
                        <div class="row">
                            <div class="col-md-6">
                            <asp:Button ID="btn_Save" runat="server" OnClientClick="return PWSMATCH()" Text="Save" CssClass="btn btn-uc btn-dark btn-block btn-lg" style="padding: 6px !important;"/>
                                </div>
                            <div class="col-md-6">
                <asp:Button ID="Cancel" runat="server"  Text="Cancel" CssClass="btn btn-uc btn-dark btn-block btn-lg" style="padding: 6px !important;"/>
                                </div>
                     </div>

                         <asp:HiddenField ID="oldpasshndfld" runat="server" />

                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>



</asp:Content>

